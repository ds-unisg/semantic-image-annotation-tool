/*
 * ==========================License-Start=============================
 * annotation-server : RDFToJSON
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.core.converter.rdf;

import de.uni_passau.fim.semantic_web.annotations.image.server.core.AnnotationCore;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.AnnotationCoreServiceFeature;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.converter.Converter;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.ontology.Ontology;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.ontology.OntologyClass;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.reasoner.Node;
import org.semanticweb.owlapi.reasoner.NodeSet;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.structural.StructuralReasonerFactory;
import org.semanticweb.owlapi.search.EntitySearcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.*;

/**
 * This class converts an OWL owlOntology into the server-internal representation of an owlOntology
 * ({@link Ontology}).
 */
public class RDFToJSON extends AnnotationCoreServiceFeature implements Converter<InputStream, Ontology> {

    private final Logger log = LoggerFactory.getLogger(this.getClass().getName());

    private final OWLOntologyManager OWL_ONTOLOGY_MANAGER = OWLManager.createOWLOntologyManager();

    private OWLOntology owlOntology;
    private OWLReasoner assertedReasoner;
    //private OWLReasoner inferredReasoner;

    private Ontology target;
    private Set<OntologyClass> classPool;

    public RDFToJSON(AnnotationCore core) {
        super(core);
    }

    /**
     * Converts an input stream containing an owlOntology to a {@link Ontology} class hierarchy
     * representation.
     *
     * @param input {@link InputStream} containing the required data to build an {@link Ontology}.
     * @return {@link Ontology} representation of the supplied owlOntology.
     */
    @Override
    public Ontology convert(InputStream input) {
        target = new Ontology.Builder().build();
        this.classPool = new TreeSet<>();

        try {
            owlOntology = OWL_ONTOLOGY_MANAGER.loadOntologyFromOntologyDocument(input);
            assertedReasoner = new StructuralReasonerFactory().createReasoner(owlOntology);
            //inferredReasoner = new Reasoner.ReasonerFactory().createReasoner(owlOntology);
        } catch (OWLOntologyCreationException e) {
            log.error("Could not create internal Ontology from OWL file");
            log.info("Exception:", e);
            log.debug("Stacktrace:", (Object[]) e.getStackTrace());
        }

        // traverseOntology(inferredReasoner);
        traverseOntology(assertedReasoner);

        OWL_ONTOLOGY_MANAGER.clearOntologies();
        return target;
    }

    private void traverseOntology(OWLReasoner reasoner) {
        final Node<OWLClass> root = reasoner.getTopClassNode();

        OntologyClass rootConvert = convertOWLClassToOntologyClass(root.getRepresentativeElement());
        classPool.add(rootConvert);

        target.addOntologyChildClass(rootConvert);

        traverseOntology(reasoner, rootConvert, root, new ArrayList<>());
    }

    private void traverseOntology(OWLReasoner reasoner, OntologyClass currentChild,
                                  Node<OWLClass> newRoot, List<Node<OWLClass>> visited) {
        if (newRoot == null || newRoot.getRepresentativeElement().isOWLNothing()) {
            return;
        }

        NodeSet<OWLClass> children = reasoner.getSubClasses(newRoot.getRepresentativeElement(), true);

        for (Node<OWLClass> childEntity : children) {
            OWLClass childRepr = childEntity.getRepresentativeElement();

            final OntologyClass childConvert = convertOWLClassToOntologyClass(childRepr);

            // Check if class already exists in class pool.
            // If so, do not instantiate a new OntologyClass object but use the existing one
            // from the class pool.
            Optional<OntologyClass> alreadyPresent = classPool
                .stream()
                .filter(clazz -> clazz.equals(childConvert))
                .findFirst();

            final OntologyClass newClass = alreadyPresent.orElseGet(() -> {
                classPool.add(childConvert);
                return childConvert;
            });

            // Do not add "nothing" to class hierarchy.
            if (!childRepr.isOWLNothing()) {
                currentChild.addChildClass(newClass);
            }

            // Recursion...
            visited.add(childEntity);
            traverseOntology(reasoner, childConvert, childEntity, visited);
            visited.remove(childEntity);
        }
    }

    private OntologyClass convertOWLClassToOntologyClass(OWLClass clazz) {
        final OntologyClass target = new OntologyClass.Builder()
            .className(clazz.getIRI().getFragment())
            .classIri(clazz.getIRI().getIRIString())
            .id(this.core.getIdFactory().createId())
            .build();

        EntitySearcher.getAnnotations(clazz, owlOntology).forEach((annotation -> {
            OWLAnnotationValue val = annotation.getValue();

            if (annotation.getProperty().isLabel()) {
                if (val instanceof OWLLiteral) {
                    target.addLiteral(((OWLLiteral) val).getLang(), ((OWLLiteral) val).getLiteral());
                }
            } else if (annotation.getProperty().isComment()) {
                if (val instanceof OWLLiteral) {
                    target.addComment(((OWLLiteral) val).getLang(), ((OWLLiteral) val).getLiteral());
                }
            } else {
                // TODO: Catchall for everything that is neither a label nor a comment.
                log.debug(MARKER, "During parsing the owlOntology annotations, neither a label nor a comment is found on property.");
            }
        }));

        return target;
    }
}
