package de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.ontology.mongodb.mapper.deserialize;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.ontology.FlatOntology;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image.annotations.mongodb.MongoId;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image.annotations.mongodb.mapper.deserialize.AnnotationDeserializer;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.ontology.mongodb.mapper.OntologyMongoModule;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Optional;

public class FlatOntologyDeserializer<T extends FlatOntology> extends JsonDeserializer<T> {
    protected final static Marker MARKER = MarkerFactory.getMarker("json");
    private final Logger log = LoggerFactory.getLogger(AnnotationDeserializer.class);

    protected ObjectMapper mapper;

    protected ObjectMapper getMapper() {
        if (this.mapper == null) {
            this.mapper = new ObjectMapper();
            mapper.registerModule(new OntologyMongoModule());
        }
        return this.mapper;
    }

    public T deserializeSuper(JsonNode root) throws IOException {
        // Search for the correct Builder.
        FlatOntology.Builder builder = null;

        Type target = ((ParameterizedType) getClass()
            .getGenericSuperclass()).getActualTypeArguments()[0];

        Class<T> builderParent = target.getTypeName().equals("T") ?
            (Class<T>) FlatOntology.class : (Class<T>) target;

        Optional<Class<?>> builderClass = Arrays.stream(builderParent.getDeclaredClasses()).filter(clazz -> {
            if (clazz.getSimpleName().equals("Builder")) {
                return true;
            } else {
                return false;
            }
        }).findFirst();

        if (builderClass.isPresent()) {
            try {
                builder = (FlatOntology.Builder) builderClass.get().newInstance();
            } catch (IllegalAccessException|InstantiationException e) {
                e.printStackTrace();
            }
        }

        builder.ontologyName(root.get("ontologyName").asText());
        builder.id(new MongoId(getMapper().treeToValue(root.get("_id"), ObjectId.class)));

        return (T) builder.build();
    }

    @Override
    public T deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        final ObjectCodec codec = jsonParser.getCodec();
        final JsonNode root = codec.readTree(jsonParser);
        return deserializeSuper(root);
    }
}
