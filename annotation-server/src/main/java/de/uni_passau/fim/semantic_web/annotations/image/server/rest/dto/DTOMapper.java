package de.uni_passau.fim.semantic_web.annotations.image.server.rest.dto;

import de.uni_passau.fim.semantic_web.annotations.image.server.core.AnnotationCore;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.annotation.Annotation;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.annotation.shapes.AnnotationCircle;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.annotation.shapes.AnnotationPolygon;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.annotation.shapes.AnnotationRectangle;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.image.AnnotatedImage;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.image.Image;
import de.uni_passau.fim.semantic_web.annotations.image.server.rest.AnnotationRESTServer;
import de.uni_passau.fim.semantic_web.annotations.image.server.rest.dto.annotations.AnnotationCircleDTO;
import de.uni_passau.fim.semantic_web.annotations.image.server.rest.dto.annotations.AnnotationDTO;
import de.uni_passau.fim.semantic_web.annotations.image.server.rest.dto.annotations.AnnotationPolygonDTO;
import de.uni_passau.fim.semantic_web.annotations.image.server.rest.dto.annotations.AnnotationRectangleDTO;
import de.uni_passau.fim.semantic_web.annotations.image.server.rest.dto.image.AnnotatedImageDTO;
import de.uni_passau.fim.semantic_web.annotations.image.server.rest.dto.image.ImageDTO;
import de.uni_passau.fim.semantic_web.annotations.image.server.rest.util.AnnotationRESTFeature;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.config.Configuration;
import org.modelmapper.convention.NameTransformers;
import org.modelmapper.convention.NamingConventions;

public class DTOMapper extends AnnotationRESTFeature {
    private transient final ModelMapper mapper = new ModelMapper();

    public DTOMapper(AnnotationCore core, AnnotationRESTServer server) {
        super(core, server);
        DTO.mapper = this;
        configure();
    }

    /**
     * Gets the configured {@link ModelMapper} instance.
     * @return Configured {@link ModelMapper} instance.
     */
    public ModelMapper mapper() {
        return mapper;
    }

    /**
     * Contains custom configuration for ModelMapper.
     */
    private void configure() {
        Configuration builderConfig = mapper.getConfiguration().copy()
            .setDestinationNameTransformer(NameTransformers.builder())
            .setDestinationNamingConvention(NamingConventions.builder());

        // ImageDTO => Image
        mapper.createTypeMap(ImageDTO.class, Image.Builder.class, builderConfig);
        mapper.createTypeMap(AnnotatedImageDTO.class, AnnotatedImage.Builder.class, builderConfig);

        // Annotation => AnnotationDTO
        mapper.createTypeMap(Annotation.class, AnnotationDTO.class);
        mapper.createTypeMap(AnnotationRectangle.class, AnnotationRectangleDTO.class)
            .includeBase(Annotation.class, AnnotationDTO.class);
        mapper.createTypeMap(AnnotationCircle.class, AnnotationCircleDTO.class)
            .includeBase(Annotation.class, AnnotationDTO.class);
        mapper.createTypeMap(AnnotationPolygon.class, AnnotationPolygonDTO.class)
            .includeBase(Annotation.class, AnnotationDTO.class);

        mapper.typeMap(AnnotationCircle.class, AnnotationDTO.class)
            .setProvider(provisionRequest -> {
                AnnotationCircleDTO target = new AnnotationCircleDTO();
                mapper.map(provisionRequest.getSource(), target);
                return target;
            })
            .addMappings(new OntologyInformationPropertyMap<>());

        mapper.typeMap(AnnotationRectangle.class, AnnotationDTO.class)
            .setProvider(provisionRequest -> {
                AnnotationRectangleDTO target = new AnnotationRectangleDTO();
                mapper.map(provisionRequest.getSource(), target);
                return target;
            })
            .addMappings(new OntologyInformationPropertyMap<>());

        mapper.typeMap(AnnotationPolygon.class, AnnotationDTO.class)
            .setProvider(provisionRequest -> {
                AnnotationPolygonDTO target = new AnnotationPolygonDTO();
                mapper.map(provisionRequest.getSource(), target);
                return target;
            })
            .addMappings(new OntologyInformationPropertyMap<>());

        // AnnotationDTO => Annotation
        mapper.createTypeMap(AnnotationDTO.class, Annotation.Builder.class, builderConfig);
        mapper.createTypeMap(AnnotationRectangleDTO.class, AnnotationRectangle.Builder.class, builderConfig)
            .includeBase(AnnotationDTO.class, Annotation.Builder.class);
        mapper.createTypeMap(AnnotationCircleDTO.class, AnnotationCircle.Builder.class, builderConfig)
            .includeBase(AnnotationDTO.class, Annotation.Builder.class);
        mapper.createTypeMap(AnnotationPolygonDTO.class, AnnotationPolygon.Builder.class, builderConfig)
            .includeBase(AnnotationDTO.class, Annotation.Builder.class);

        mapper.typeMap(Annotation.class, AnnotationDTO.class)
            .addMappings(new OntologyInformationPropertyMap<>());

        mapper.typeMap(AnnotationRectangleDTO.class, AnnotationRectangle.Builder.class)
            .setProvider(provisionRequest -> {
                AnnotationRectangle.Builder builder = new AnnotationRectangle.Builder();
                AnnotationRectangleDTO source = (AnnotationRectangleDTO) provisionRequest.getSource();
                mapper.map(source, builder);

                if (source.getOntologyId() != null) {
                    builder.ontology(core.getOntologyStorage()
                        .getOntology(
                            core.getIdFactory().createId(source.getOntologyId()))
                    );
                }

                if (source.getOntologyClassId() != null) {
                    builder.ontologyClass(core.getOntologyStorage()
                        .getOntologyClassById(
                            core.getIdFactory().createId(source.getOntologyId()),
                            core.getIdFactory().createId(source.getOntologyClassId()))
                    );
                }

                return builder;
            });
    }

    /**
     * {@link PropertyMap} configuration used for ontology information mapping (complex objects to String ids).
     *
     * @param <S> Source type.
     * @param <D> Destination DTO type.
     */
    private static class OntologyInformationPropertyMap<S extends Annotation, D extends AnnotationDTO>
        extends PropertyMap<S, D> {

        @Override
        protected void configure() {
            map().setOntologyId(source.getOntology().getId().toString());
            map().setOntologyClassId(source.getOntologyClass().getId().toString());
            map().setOntologyClassName(source.getOntologyClass().getClassName());
            map().setOntologyClassIri(source.getOntologyClass().getClassIri());
        }
    }

}
