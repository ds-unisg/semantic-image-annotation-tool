/*
 * ==========================License-Start=============================
 * annotation-server : ImporterResult
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.core.importer;

import java.util.Collections;
import java.util.List;

/**
 * Simple data class that contains the results after an import has finished.
 */
public class ImporterResult {
    private final int successfullyImported;
    private final int failedImports;
    private final int unknownFiles;
    private final List<String> failedImportImageData;
    private final List<String> failedImportJsonData;

    public ImporterResult(int successfullyImported, int failedImports, int unknownFiles,
                          List<String> failedImportImageData, List<String> failedImportJsonData) {
        this.successfullyImported = successfullyImported;
        this.failedImports = failedImports;
        this.unknownFiles = unknownFiles;
        this.failedImportImageData = failedImportImageData;
        this.failedImportJsonData = failedImportJsonData;
    }

    public int getSuccessfullyImported() {
        return successfullyImported;
    }

    public int getFailedImports() {
        return failedImports;
    }

    public List<String> getFailedImportImageData() {
        return Collections.unmodifiableList(failedImportImageData);
    }

    public List<String> getFailedImportJsonData() {
        return Collections.unmodifiableList(failedImportJsonData);
    }

    public int getUnknownFiles() {
        return unknownFiles;
    }
}
