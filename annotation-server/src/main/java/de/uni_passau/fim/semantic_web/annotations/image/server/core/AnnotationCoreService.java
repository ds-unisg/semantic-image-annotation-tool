/*
 * ==========================License-Start=============================
 * annotation-server : AnnotationCoreService
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.core;

import com.typesafe.config.Config;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.automatic_annotation.AutomaticAnnotationService;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.bgtask.BackgroundTaskManager;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.IdFactory;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.importer.ImportManager;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image.ImageStorage;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image.ImageStorageImpl;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.ontology.OntologyStorage;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.ontology.mongodb.MongoIdFactory;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.ontology.mongodb.MongoOntologyStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AnnotationCoreService extends AnnotationCore {

    @SuppressWarnings("FieldCanBeLocal")
    private final Logger log = LoggerFactory.getLogger(AnnotationCoreService.class);

    private final ImageStorage imageStorage;
    private final OntologyStorage ontologyStorage;
    private final AutomaticAnnotationService automaticAnnotationService;
    private final ImportManager importManager;
    private final BackgroundTaskManager backgroundTaskManager;
    private final IdFactory idFactory;

    public AnnotationCoreService(Config config) {
        super(config);
        log.debug(MARKER, "Adding ImageStorage");
        this.imageStorage = new ImageStorageImpl(this);
        log.debug(MARKER, "Adding OntologyStorage");
        this.ontologyStorage = new MongoOntologyStorage(this);
        log.debug(MARKER, "Adding AutomaticAnnotationService");
        this.automaticAnnotationService = new AutomaticAnnotationService(this);
        log.debug(MARKER, "Adding ImportManager");
        this.importManager = new ImportManager(this);
        log.debug(MARKER, "Adding BackgroundTaskManager");
        this.backgroundTaskManager = new BackgroundTaskManager(this);
        log.debug(MARKER, "Adding ImportManager");
        this.idFactory = new MongoIdFactory(this);

        log.debug(MARKER, "AnnotationCoreService fully initialized.");
    }

    @Override
    public ImageStorage getImageStorage() {
        return this.imageStorage;
    }

    @Override
    public OntologyStorage getOntologyStorage() {
        return ontologyStorage;
    }

    @Override
    public AutomaticAnnotationService getAutomaticAnnotationService() {
        return automaticAnnotationService;
    }

    @Override
    public ImportManager getImportManager() {
        return importManager;
    }

    public IdFactory getIdFactory() {
        return idFactory;
    }

    public BackgroundTaskManager getBackgroundTaskManager() {
        return backgroundTaskManager;
    }
}
