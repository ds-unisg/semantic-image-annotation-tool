/*
 * ==========================License-Start=============================
 * annotation-server : NoImageProvidedException
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.rest.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * This exception is thrown within the REST component of this application if an image upload
 * is being requested but no image data is attached to the request.
 */
@Provider
public class NoImageProvidedException extends WebApplicationException
    implements ExceptionMapper<NoImageProvidedException> {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private String msg;

    /**
     * Creates a new instance. No error message text is set.
     */
    public NoImageProvidedException() {
    }

    /**
     * Creates a new instance with an error message set.
     *
     * @param msg Error message text that should get set.
     */
    public NoImageProvidedException(String msg) {
        this.msg = msg;
    }

    /**
     * Gets the error message text.
     *
     * @return The error message text.
     */
    public String getMsg() {
        return msg;
    }

    /**
     * Sets the error message text.
     *
     * @param msg The error message text to be set.
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Response toResponse(NoImageProvidedException e) {
        logger.error("NoImageProvidedException caught: " + e.getMessage());
        return Response.status(400).entity(e).build();
    }
}
