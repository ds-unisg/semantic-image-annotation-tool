/*
 * ==========================License-Start=============================
 * annotation-server : ImageStorage
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image;

import de.uni_passau.fim.semantic_web.annotations.image.server.core.AnnotationCore;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.AnnotationCoreServiceFeature;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.Id;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.image.AnnotatedImage;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.image.Image;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.annotation.Annotation;

import java.util.List;
import java.util.stream.Stream;

public abstract class ImageStorage<T extends Id> extends AnnotationCoreServiceFeature {

    ImageStorage(AnnotationCore core) {
        super(core);
    }

    public abstract Stream<Image> getImages();

    public abstract AnnotatedImage getImageAnnotations(T id);

    public abstract byte[] getImage(T id);

    public abstract byte[] getImageThumbnail(T id);

    public abstract void updateImage(T id, byte[] data);

    public abstract void updateImage(T id, List<Annotation> annotations);

    public abstract AnnotatedImage uploadImage(byte[] imgData, List<Annotation> annotations, String filename);

    public abstract void deleteImage(T id);

}
