/*
 * ==========================License-Start=============================
 * annotation-server : MongoOntologyStorage
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.ontology.mongodb;

import com.fasterxml.jackson.databind.MapperFeature;
import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.typesafe.config.Config;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.AnnotationCore;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.converter.Converter;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.converter.rdf.RDFToJSON;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.image.AnnotatedImage;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.ontology.FlatOntology;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.ontology.Ontology;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.ontology.OntologyClass;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.exceptions.EntityNotFoundException;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.exceptions.OntologyLoadingException;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image.annotations.mongodb.MongoId;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.ontology.OntologyStorage;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.ontology.mongodb.mapper.OntologyMongoModule;
import org.jongo.Jongo;
import org.jongo.MongoCollection;
import org.jongo.MongoCursor;
import org.jongo.marshall.jackson.JacksonMapper;
import org.semanticweb.owlapi.model.UnloadableImportException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class MongoOntologyStorage extends OntologyStorage<MongoId> {

    private final Logger log = LoggerFactory.getLogger(MongoOntologyStorage.class);

    private final MongoCollection ontologies;

    public MongoOntologyStorage(AnnotationCore core) {
        super(core);

        Config db_config = core.getConfig().getConfig("mongodb");
        MongoClient connection = new MongoClient(db_config.getString("host"), db_config.getInt("port"));
        @SuppressWarnings("deprecation") DB db = connection.getDB(db_config.getString("db.name"));
        Jongo jongo = new Jongo(db, new JacksonMapper.Builder()
            .registerModule(new OntologyMongoModule())
            .enable(MapperFeature.AUTO_DETECT_GETTERS)
            .build());
        ontologies = jongo.getCollection("ontologies");
    }

    @Override
    public List<FlatOntology> getOntologyList() {
        MongoCursor<FlatOntology> res = ontologies.find().as(FlatOntology.class);

        List<FlatOntology> list = new ArrayList<>();
        res.forEach(list::add);
        return list;
    }

    @Override
    public Ontology getOntology(MongoId id) {
        if (id == null || id.getId() == null) {
            return null;
        }
        return ontologies.findOne(id.getId()).as(Ontology.class);
    }

    @Override
    public Ontology uploadOntology(String name, InputStream upl) {
        Converter<InputStream, Ontology> rdfToJSONConverter = new RDFToJSON(this.core);
        try {
            final Ontology ontology = new Ontology.Builder<>()
                .ontologyChildren(rdfToJSONConverter.convert(upl).getOntologyChildren())
                .ontologyName(name)
                .date(Instant.now())
                .id(this.core.getIdFactory().createId())
                .build();
            ontologies.insert(ontology);

            return ontology;
        } catch (UnloadableImportException e) {
            throw new OntologyLoadingException(e);
        } finally {
            try {
                upl.close();
            } catch (IOException e) {
                log.error(MARKER, "Could not close InputStream of uploaded ontology file");
            }
        }
    }

    @Override
    public boolean deleteOntology(MongoId id) {
        boolean success = false;
        if (id != null) {
            log.info(MARKER, "Trying to delete ontology {}", id.getId().toString());
            success = ontologies.remove(id.getId()).wasAcknowledged();

            if (!success) {
                throw new EntityNotFoundException("Failed to delete ontology "
                    + "- perhaps it is not present in the database?");
            }

            log.info(MARKER, "Deletion was successful: {}", success);
        } else {
            throw new IllegalArgumentException("ID must not be null!");
        }
        return success;
    }

    @Override
    public OntologyClass getOntologyClassByName(MongoId ontologyId, String className) {
        log.info(MARKER, "Client requested ontology class by name {} for ontology {}",
            className, ontologyId.getId());

        return ontologies.findOne(ontologyId.getId()).as(Ontology.class)
            .findClassByName(className).orElse(null);

        //return ontologies.findOne("{\"ontologyClasses.ontologyClass\" : \"" + ontologyClass +"\"}")
        //    .as(OntologyClass.class);
    }

    @Override
    public OntologyClass getOntologyClassById(MongoId ontologyId, MongoId classId) {
        log.info(MARKER, "Client requested ontology class by id {} for ontology {}",
            classId.getId(), ontologyId.getId());

        System.out.println(ontologyId);
        System.out.println(classId);

        return ontologies.findOne(ontologyId.getId()).as(Ontology.class)
            .findClassById(classId).orElse(null);
        
        //return ontologies.findOne(classId.getId()).as(OntologyClass.class);
    }
}
