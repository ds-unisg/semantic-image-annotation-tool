package de.uni_passau.fim.semantic_web.annotations.image.server.rest.dto.image;

import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.image.Image;
import de.uni_passau.fim.semantic_web.annotations.image.server.rest.dto.DTO;

public class ImageDTO<T extends ImageDTO> extends DTO<Image, T> {
    protected String id;
    protected String filename;
    protected ImageSizeDTO size;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ImageSizeDTO getSize() {
        return size;
    }

    public void setSize(ImageSizeDTO size) {
        this.size = size;
    }

    @Override
    public Image toConcrete() {
        Image target = new Image.Builder<>().build();
        mapper().map(this, target);
        return target;
    }

    @Override
    public T fromConcrete(Image concrete) {
        mapper().map(concrete, this);
        return (T) this;
    }

    public static class ImageSizeDTO extends DTO<Image.Size, ImageSizeDTO> {
        private int width;
        private int height;

        public ImageSizeDTO(int width, int height) {
            this.width = width;
            this.height = height;
        }

        public ImageSizeDTO() { }

        public int getWidth() {
            return width;
        }

        public void setWidth(int width) {
            this.width = width;
        }

        public int getHeight() {
            return height;
        }

        public void setHeight(int height) {
            this.height = height;
        }

        @Override
        public Image.Size toConcrete() {
            Image.Size target = new Image.Size(
                width,
                height
            );

            return target;
        }

        @Override
        public ImageSizeDTO fromConcrete(Image.Size concrete) {
            return new ImageSizeDTO(concrete.getWidth(), concrete.getHeight());
        }
    }
}
