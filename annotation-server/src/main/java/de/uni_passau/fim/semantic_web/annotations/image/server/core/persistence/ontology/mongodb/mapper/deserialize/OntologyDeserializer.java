/*
 * ==========================License-Start=============================
 * annotation-server : OntologyDeserializer
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.ontology.mongodb.mapper.deserialize;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.Id;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.ontology.FlatOntology;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.ontology.Ontology;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.ontology.OntologyClass;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image.annotations.mongodb.MongoId;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.ontology.mongodb.mapper.OntologyMongoModule;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class OntologyDeserializer extends FlatOntologyDeserializer<Ontology> {
    private static final Marker MARKER = MarkerFactory.getMarker("json");

    private final Logger log = LoggerFactory.getLogger(OntologyDeserializer.class);

    @Override
    public Ontology deserialize(JsonParser jsonParser,
                                DeserializationContext deserializationContext)
        throws IOException {

        final ObjectCodec codec = jsonParser.getCodec();
        final JsonNode root = codec.readTree(jsonParser);

        Ontology baseOntology = super.deserializeSuper(root);

        final Map<Id, OntologyClass> classPool = new ConcurrentHashMap<>();
        final Map<OntologyClass, List<Id>> subclassMapping = new ConcurrentHashMap<>();

        // Create class pool from json.
        StreamSupport
            .stream(root.get("ontologyClasses").spliterator(), true)
            .collect(Collectors.toList())
            .parallelStream()
            .forEach(ontologyClassJsonNode -> {
            try {
                final OntologyClass clazz = getMapper().treeToValue(ontologyClassJsonNode, OntologyClass.class);
                classPool.put(clazz.getId(), clazz);

                // Fetch mapping
                final List<ObjectId> subclassIds = new ArrayList<>();
                ontologyClassJsonNode.get("subclasses")
                    .forEach(subClassId -> {
                        try {
                            subclassIds.add(getMapper().treeToValue(subClassId, ObjectId.class));
                        } catch (JsonProcessingException e) {
                            log.error(MARKER, "Cannot deserialize subclass.");
                        }
                    });

                subclassMapping.put(clazz, subclassIds
                    .parallelStream()
                    .map(MongoId::new)
                    .collect(Collectors.toList())
                );
            } catch (JsonProcessingException e) {
                log.error(MARKER, "Cannot create JSON from OntologyClasses for Ontology.", e);
            }
        });

        // Map subclasses
        classPool.forEach((id, clazz) -> {

            final SortedSet<OntologyClass> subclassIds = subclassMapping
                .get(clazz)
                .parallelStream()
                .map(classPool::get)
                .collect(Collectors.toCollection(TreeSet::new));

            clazz.setSubClasses(subclassIds);
        });

        // Finally, set all root classes.
        final SortedSet<OntologyClass> rootClasses = new TreeSet<>();

        root.get("rootClasses")
            .forEach(
                rootClass -> {
                    try {
                        rootClasses.add(classPool.get(getMapper().treeToValue(rootClass, OntologyClass.class).getId()));
                    } catch (JsonProcessingException e) {
                        log.error(MARKER, "Cannot create JSON from RootClasses for Ontology.", e);
                    }

                });

        baseOntology.setOntologyChildren(rootClasses);
        return baseOntology;
    }
}
