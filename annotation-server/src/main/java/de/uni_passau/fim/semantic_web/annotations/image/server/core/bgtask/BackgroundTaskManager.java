package de.uni_passau.fim.semantic_web.annotations.image.server.core.bgtask;

import de.uni_passau.fim.semantic_web.annotations.image.server.core.AnnotationCore;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.AnnotationCoreServiceFeature;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.Id;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class BackgroundTaskManager extends AnnotationCoreServiceFeature {
    private final Logger log = LoggerFactory.getLogger(BackgroundTaskManager.class);

    private Queue<BackgroundTask> queue;
    private transient Thread managerThread;

    public BackgroundTaskManager(final AnnotationCore core) {
        super(core);
        this.queue = new ConcurrentLinkedQueue<>();
        managerThread = new Thread(new BackgroundTaskManagerThread());
    }

    public BackgroundTask enqueue(final Runnable r) {
        BackgroundTask bgTask = new BackgroundTask.Builder<>()
            .task(r)
            .id(core.getIdFactory().createId())
            .build();

        queue.add(bgTask);

        log.info("New BackgroundTaskDTO enqueued with id {}", bgTask.getId());

        if (managerThread.getState() == Thread.State.TERMINATED) {
            managerThread = new Thread(new BackgroundTaskManagerThread());
            managerThread.run();
        }
        else if (!managerThread.isAlive()) { // On first run
            managerThread.start();
        }

        return bgTask;
    }

    public Optional<BackgroundTask> getTask(Id id) {
        log.info("Fetching BackgroundTaskDTO with id {}", id);
        return queue.stream().filter(it -> it.getId().equals(id)).findFirst();
    }

    public boolean removeTask(final BackgroundTask bgTask) {
        log.info("Trying to delete BackgroundTaskDTO with id {}", bgTask.getId());
        return queue.remove(bgTask);
    }

    public List<BackgroundTask> getTaskQueue() {
        log.info("Answering task list request.");
        return new ArrayList<>(queue);
    }

    private class BackgroundTaskManagerThread implements Runnable {
        @Override
        public void run() {
            while(!queue.isEmpty()) {
                BackgroundTask next = queue.poll();
                next.getTask().run();
            }
        }
    }
}
