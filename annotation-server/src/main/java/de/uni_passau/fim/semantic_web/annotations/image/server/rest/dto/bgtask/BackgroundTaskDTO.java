package de.uni_passau.fim.semantic_web.annotations.image.server.rest.dto.bgtask;

import de.uni_passau.fim.semantic_web.annotations.image.server.core.bgtask.BackgroundTask;
import de.uni_passau.fim.semantic_web.annotations.image.server.rest.dto.DTO;

public class BackgroundTaskDTO extends DTO<BackgroundTask, BackgroundTaskDTO> {
    public String id;

    public BackgroundTaskDTO() { }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public BackgroundTask toConcrete() {
        return mapper().map(this, BackgroundTask.class);
    }

    @Override
    public BackgroundTaskDTO fromConcrete(BackgroundTask concrete) {
        mapper().map(concrete, this);
        return this;
    }
}
