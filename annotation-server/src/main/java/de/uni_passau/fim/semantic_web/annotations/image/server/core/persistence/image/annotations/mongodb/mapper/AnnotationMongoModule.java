/*
 * ==========================License-Start=============================
 * annotation-server : AnnotationMongoModule
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image.annotations.mongodb.mapper;

import com.fasterxml.jackson.core.json.PackageVersion;
import com.fasterxml.jackson.databind.module.SimpleModule;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.AnnotationCore;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.image.AnnotatedImage;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.image.Image;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.annotation.shapes.AnnotationPolygon;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.annotation.shapes.AnnotationRectangle;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image.annotations.mongodb.mapper.deserialize.AnnotatedImageDeserializer;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image.annotations.mongodb.mapper.deserialize.AnnotationPolygonDeserializer;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image.annotations.mongodb.mapper.deserialize.AnnotationRectangleDeserializer;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image.annotations.mongodb.mapper.deserialize.ImageDeserializer;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image.annotations.mongodb.mapper.serialize.AnnotatedImageSerializer;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image.annotations.mongodb.mapper.serialize.AnnotationPolygonSerializer;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image.annotations.mongodb.mapper.serialize.AnnotationRectangleSerializer;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image.annotations.mongodb.mapper.serialize.ImageSerializer;

import java.util.LinkedList;

public class AnnotationMongoModule extends SimpleModule {

    public AnnotationMongoModule(AnnotationCore core) {
        super(PackageVersion.VERSION);

        // Register (de)serializer for Annotation class.
        addDeserializer(AnnotationRectangle.class, new AnnotationRectangleDeserializer(core));
        addSerializer(AnnotationPolygon.class, new AnnotationPolygonSerializer(core));
        //addSerializer(AnnotationCircle.class, new AnnotationCircleSerializer(core));
        //addDeserializer(AnnotationCircle.class, new AnnotationCircleDeserializer(core));
        addSerializer(AnnotationRectangle.class, new AnnotationRectangleSerializer(core));
        addDeserializer(AnnotationPolygon.class, new AnnotationPolygonDeserializer(core));

        // Register (de)serializer for Image class.
        addDeserializer(Image.class, new ImageDeserializer(core));
        addDeserializer(AnnotatedImage.class, new AnnotatedImageDeserializer(core));

        addSerializer(AnnotatedImage.class, new AnnotatedImageSerializer(core));
    }

    @Override
    public String getModuleName() {
        return getClass().getSimpleName();
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }
}
