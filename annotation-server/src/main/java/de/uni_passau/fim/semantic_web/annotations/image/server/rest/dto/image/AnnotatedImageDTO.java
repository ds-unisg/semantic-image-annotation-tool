package de.uni_passau.fim.semantic_web.annotations.image.server.rest.dto.image;

import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.image.AnnotatedImage;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.image.Image;
import de.uni_passau.fim.semantic_web.annotations.image.server.rest.dto.annotations.AnnotationDTO;

import java.util.List;

public class AnnotatedImageDTO extends ImageDTO<AnnotatedImageDTO> {
    private List<AnnotationDTO> annotations;

    public AnnotatedImageDTO() {}

    public List<AnnotationDTO> getAnnotations() {
        return annotations;
    }

    public void setAnnotations(List<AnnotationDTO> annotations) {
        this.annotations = annotations;
    }

    @Override
    public Image toConcrete() {
        AnnotatedImage target = new AnnotatedImage.Builder<>().build();
        mapper().map(this, target);
        return target;
    }

    @Override
    public AnnotatedImageDTO fromConcrete(Image concrete) {
        if (!(concrete instanceof AnnotatedImage)) {
            throw new IllegalArgumentException("Image must be of type AnnotatedImage!");
        }

        mapper().map(concrete, this);
        return this;
    }
}
