/*
 * ==========================License-Start=============================
 * annotation-server : Annotation
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.annotation;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.Entity;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.EntityBuilder;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.Id;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.annotation.shapes.AnnotationCircle;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.annotation.shapes.AnnotationPolygon;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.annotation.shapes.AnnotationRectangle;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.ontology.Ontology;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.ontology.OntologyClass;

import javax.validation.constraints.NotNull;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")

@JsonSubTypes({
    @JsonSubTypes.Type(value = AnnotationCircle.class, name = "circle"),
    @JsonSubTypes.Type(value = AnnotationPolygon.class, name = "polygon"),
    @JsonSubTypes.Type(value = AnnotationRectangle.class, name = "rectangle")
})

@JsonIgnoreProperties(ignoreUnknown = true)

public abstract class Annotation extends Entity<Id> {

    @NotNull
    private OntologyClass ontologyClass;

    @NotNull
    private Ontology ontology;

    @NotNull
    private String fill;

    /**
     * Creates a new annotation with the ontology class and ontology context set.
     *
     * @param builder Ontology class this annotation refers to.
     */
    protected Annotation(Annotation.Builder builder) {
        super(builder);
        this.ontologyClass = builder.ontologyClass;
        this.ontology = builder.ontology;
        this.fill = builder.fill;
    }

    /**
     * Gets the ontology class this annotation refers to.
     *
     * @return Ontology class this annotation refers to.
     */
    public OntologyClass getOntologyClass() {
        return ontologyClass;
    }

    /**
     * Gets the ontology context of this {@link Annotation}.
     *
     * @return Ontology context of this {@link Annotation}.
     */
    public Ontology getOntology() {
        return ontology;
    }

    public String getFill() {
        return fill;
    }


    public static abstract class Builder extends EntityBuilder<Id, Annotation> {
        private OntologyClass ontologyClass;
        private Ontology ontology;
        private String fill;

        public Annotation.Builder ontologyClass(final OntologyClass className) {
            this.ontologyClass = className;
            return this;
        }

        public Annotation.Builder ontology(final Ontology ontologyContext) {
            this.ontology = ontologyContext;
            return this;
        }

        public Annotation.Builder fill(final String fill) {
            this.fill = fill;
            return this;
        }

        public abstract Annotation build();
    }
}
