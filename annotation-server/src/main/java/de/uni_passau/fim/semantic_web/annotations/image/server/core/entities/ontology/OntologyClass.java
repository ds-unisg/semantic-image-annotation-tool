/*
 * ==========================License-Start=============================
 * annotation-server : OntologyClass
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.ontology;

import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.Entity;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.EntityBuilder;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.Id;

import javax.annotation.Nonnull;
import java.util.*;

/**
 * This class is used to represent an ontology class in a more primitive way.
 * This data is intended to be used for frontend purposes.
 * <p>
 * Ontology classes are identified by their class name.
 */
public class OntologyClass extends Entity<Id> implements Comparable<OntologyClass> {
    private String className;
    private String classIri;
    private SortedMap<String, String> literals;
    private SortedMap<String, String> comments;
    private SortedSet<OntologyClass> subClasses;

    /**
     * Creates an empty, new {@link OntologyClass} with its class name set.
     *
     * @param builder Builder
     */
    public OntologyClass(OntologyClass.Builder builder) {
        super(builder);
        this.className = builder.className;
        this.classIri = builder.classIri;
        this.subClasses = Optional.ofNullable(builder.subClasses).orElse(new TreeSet<>());
        this.literals = Optional.ofNullable(builder.literals).orElse(new TreeMap<>());
        this.comments = Optional.ofNullable(builder.comments).orElse(new TreeMap<>());
    }

    /**
     * Gets the class name of this {@link OntologyClass}.
     *
     * @return Class name of this {@link OntologyClass}.
     */
    public String getClassName() {
        return className;
    }

    /**
     * Adds a new child class to this {@link OntologyClass}.
     *
     * @param a {@link OntologyClass} that should be added as a child class to this
     *          {@link OntologyClass}.
     */
    public void addChildClass(OntologyClass a) {
        subClasses.add(a);
    }

    /**
     * Deletes a child class from this {@link OntologyClass}.
     *
     * @param d {@link OntologyClass} that should be deleted as a direct child from this
     *          {@link OntologyClass}.
     * @return {@code True} if the child has been removed successfully, {@code False} if not.
     */
    public boolean deleteChildClass(OntologyClass d) {
        return subClasses.remove(d);
    }

    /**
     * Returns an unmodifiable set of all subclasses of this {@link OntologyClass}.
     *
     * @return Unmodifiable set containing all subclasses.
     */
    public SortedSet<OntologyClass> getSubClasses() {
        return Collections.unmodifiableSortedSet(subClasses);
    }

    /**
     * Sets the subclasses of this {@link OntologyClass}.
     *
     * @param subClasses {@link SortedSet} of {@link OntologyClass}es that should be set as child
     *                   classes of this {@link OntologyClass}.
     */
    public void setSubClasses(SortedSet<OntologyClass> subClasses) {
        this.subClasses = subClasses;
    }

    /**
     * Gets an unmodifiable map containing all literals of this {@link OntologyClass}.
     *
     * @return Unmodifiable map containing all literals of this {@link OntologyClass}.
     */
    public SortedMap<String, String> getLiterals() {
        return Collections.unmodifiableSortedMap(literals);
    }

    /**
     * Adds a literal to this {@link OntologyClass}.
     *
     * @param lang Language of the literal to be added.
     * @param text Text of the literal to be added.
     */
    public void addLiteral(String lang, String text) {
        literals.put(lang, text);
    }

    /**
     * Removes a literal from this {@link OntologyClass}.
     *
     * @param lang Language of the literal to be removed.
     * @param text Text of the literal to be removed.
     * @return {@code True} if the literal has been removed successfully, {@code False} if not.
     */
    public boolean removeLiteral(String lang, String text) {
        return literals.remove(lang, text);
    }

    /**
     * Gets all comments attached to this {@link OntologyClass}.
     *
     * @return Map containing all comments attached to this {@link OntologyClass}.
     */
    public SortedMap<String, String> getComments() {
        return comments;
    }

    /**
     * Adds a comment to this {@link OntologyClass}.
     *
     * @param key   Key value of the comment.
     * @param value Text of the comment.
     */
    public void addComment(String key, String value) {
        comments.put(key, value);
    }

    public String getClassIri() {
        return classIri;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int compareTo(@Nonnull OntologyClass ontologyClass) {
        return this.getClassName().compareTo(ontologyClass.getClassName());
    }

    @Override
    public int hashCode() {
        return this.className.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof OntologyClass)) {
            return false;
        } else {
            return this.className.equals(((OntologyClass) obj).className);
        }
    }

    public static class Builder extends EntityBuilder<Id, OntologyClass> {

        private String className;
        private String classIri;
        private SortedMap<String, String> literals;
        private SortedMap<String, String> comments;
        private SortedSet<OntologyClass> subClasses;

        public OntologyClass.Builder className(String className) {
            this.className = className;
            return this;
        }

        public OntologyClass.Builder classIri(String classIri) {
            this.classIri = classIri;
            return this;
        }

        public OntologyClass.Builder literals(SortedMap<String, String> literals) {
            this.literals = literals;
            return this;
        }
        public OntologyClass.Builder comments(SortedMap<String, String> comments) {
            this.comments = comments;
            return this;
        }
        public OntologyClass.Builder subClasses(SortedSet<OntologyClass> subClasses) {
            this.subClasses = subClasses;
            return this;
        }

        @Override
        public OntologyClass build() {
            return new OntologyClass(this);
        }
    }
}
