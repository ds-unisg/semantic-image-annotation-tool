package de.uni_passau.fim.semantic_web.annotations.image.server.rest.resources;

import de.uni_passau.fim.semantic_web.annotations.image.server.core.AnnotationCore;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.bgtask.BackgroundTask;
import de.uni_passau.fim.semantic_web.annotations.image.server.rest.dto.bgtask.BackgroundTaskDTO;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Optional;
import java.util.stream.Collectors;

@Path("/task")
public class BackgroundTaskResource extends AbstractResource {
    public BackgroundTaskResource(AnnotationCore core) {
        super(core);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getBackgroundTaskList() {
        return Response.ok().entity(core.getBackgroundTaskManager()
            .getTaskQueue()
            .parallelStream()
            .map(task -> new BackgroundTaskDTO().fromConcrete(task))
            .collect(Collectors.toList()))
            .build();
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/cancel/{id}")
    public Response cancelTask(@Valid @NotNull @QueryParam("id") String id) {
        Optional<BackgroundTask> target = core.getBackgroundTaskManager().getTask(
            core.getIdFactory().createId(id)
        );

        if (target.isPresent() && core.getBackgroundTaskManager().removeTask(target.get())) {
            return Response.ok().build();
        } else {
            return Response.status(Response.Status.NOT_FOUND)
                .build();
        }
    }
}
