/*
 * ==========================License-Start=============================
 * annotation-server : AnnotationPolygon
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.annotation.shapes;

import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.annotation.Annotation;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * This class defines a polygon-shaped {@link Annotation}.
 */
public class AnnotationPolygon extends Annotation {

    private List<PolygonPoint> points;

    /**
     * Creates a new, empty {@link AnnotationPolygon} instance.
     */
    private AnnotationPolygon(final AnnotationPolygon.Builder builder) {
        super(builder);
        this.points = Optional.ofNullable(builder.points).orElse(new ArrayList<>());
    }

    /**
     * Gets the points of this {@link AnnotationPolygon}.
     *
     * @return Points of this {@link AnnotationPolygon}.
     */
    public List<PolygonPoint> getPoints() {
        return points;
    }

    /**
     * This class defines a single point that is part of a polygon.
     */
    public static class PolygonPoint implements Comparable<PolygonPoint> {
        int x;
        int y;

        /**
         * Creates a new, uninitialized instance.
         */
        public PolygonPoint() {
        }

        /**
         * Gets the x-coordinate of this point.
         *
         * @return X-coordinate of this point.
         */
        public int getX() {
            return x;
        }

        /**
         * Sets the x-coordinate of this point.
         *
         * @param x x-coordinate this point should be set to.
         */
        public void setX(int x) {
            this.x = x;
        }

        /**
         * Gets the y-coordinate of this point.
         *
         * @return Y-coordinate of this point.
         */
        public int getY() {
            return y;
        }

        /**
         * Sets the y-coordinate of this point.
         *
         * @param y y-coordinate this point should be set to.
         */
        public void setY(int y) {
            this.y = y;
        }

        /**
         * Sorts all points from top-left to right-bottom.
         *
         * @param polygonPoint Polygon point to be compared.
         * @return {@code -1, 0, 1} depending on defined order.
         */
        @Override
        public int compareTo(@Nonnull PolygonPoint polygonPoint) {
            if (this.y < polygonPoint.y) {
                return -1;
            } else if (this.y == polygonPoint.y) {
                return Integer.compare(this.x, polygonPoint.x);
            } else {
                return 1;
            }
        }
    }

    public static class Builder extends Annotation.Builder {

        private List<PolygonPoint> points;

        public AnnotationPolygon.Builder points(final List<AnnotationPolygon.PolygonPoint> points) {
            this.points = points;
            return this;
        }

        @Override
        public AnnotationPolygon build() {
            return new AnnotationPolygon(this);
        }

    }
}
