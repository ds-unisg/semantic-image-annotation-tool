package de.uni_passau.fim.semantic_web.annotations.image.server.rest.dto.annotations;

import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.annotation.Annotation;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.annotation.shapes.AnnotationCircle;

public class AnnotationCircleDTO extends AnnotationDTO<AnnotationCircleDTO> {
    private int radius;

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    @Override
    public Annotation toConcrete() {
        return mapper().map(this, AnnotationCircle.Builder.class).build();
    }

    @Override
    public AnnotationCircleDTO fromConcrete(Annotation concrete) {
        mapper().map(concrete, this);
        return this;
    }
}
