package de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image.annotations.mongodb.mapper.deserialize;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.AnnotationCore;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.annotation.Annotation;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.image.AnnotatedImage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AnnotatedImageDeserializer<T extends AnnotatedImage> extends ImageDeserializer<AnnotatedImage> {
    public AnnotatedImageDeserializer(AnnotationCore core) {
        super(core);
    }

    @Override
    public T deserialize(final JsonParser jsonParser,
                         final DeserializationContext deserializationContext) throws IOException {
        // Initial JSON processing.
        final ObjectCodec codec = jsonParser.getCodec();
        final JsonNode root = codec.readTree(jsonParser);

        AnnotatedImage baseImage = super.deserializeSuper(root);

        final List<Annotation> annotations = new ArrayList<>();
        for (JsonNode jsonAnnotation : root.get("annotations")) {
            annotations.add(this.getMapper().treeToValue(jsonAnnotation, Annotation.class));
        }

        baseImage.setAnnotations(annotations);
        return (T) baseImage;
    }
}
