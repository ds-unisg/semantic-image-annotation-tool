package de.uni_passau.fim.semantic_web.annotations.image.server.rest.dto.importer;

import de.uni_passau.fim.semantic_web.annotations.image.server.core.importer.Importer;
import de.uni_passau.fim.semantic_web.annotations.image.server.rest.dto.DTO;

public class ImporterDTO extends DTO<Importer, ImporterDTO> {
    public String name;

    public ImporterDTO() { }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Importer toConcrete() {
        return mapper().map(this, Importer.class);
    }

    @Override
    public ImporterDTO fromConcrete(Importer concrete) {
        mapper().map(concrete, this);
        return this;
    }
}
