package de.uni_passau.fim.semantic_web.annotations.image.server.core.bgtask;

import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.Entity;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.EntityBuilder;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.Id;

public class BackgroundTask extends Entity<Id> {

    private final transient Runnable task;
    private final transient String name;

    private BackgroundTask(BackgroundTask.Builder builder) {
        super(builder);
        this.task = builder.task;
        this.name = builder.name;
    }

    public static class Builder<B extends BackgroundTask.Builder<B, T>, T extends BackgroundTask>
        extends EntityBuilder<Id, T> {

        protected Runnable task;
        protected String name;

        public B task(Runnable task) {
            this.task = task;
            return (B) this;
        }

        public B name(String name) {
            this.name = name;
            return (B) this;
        }

        @Override
        public T build() {
            return (T) new BackgroundTask(this);
        }
    }

    public Runnable getTask() {
        return task;
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof BackgroundTask) {
            return ((BackgroundTask) obj).getId().equals(getId());
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return String.format("Background Task with ID %s", getId());
    }
}
