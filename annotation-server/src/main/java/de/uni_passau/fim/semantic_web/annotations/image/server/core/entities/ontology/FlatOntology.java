package de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.ontology;

import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.Entity;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.EntityBuilder;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.Id;

import java.time.Instant;

public class FlatOntology extends Entity<Id> {
    private final String ontologyName;
    private final String ontologyIri;
    private final Instant date;

    public FlatOntology(FlatOntology.Builder builder) {
        super(builder);
        this.ontologyName = builder.ontologyName;
        this.ontologyIri = builder.ontologyIri;
        this.date = builder.date;
    }

    /**
     * Gets the name of this {@link Ontology}.
     *
     * @return Name of this {@link Ontology}.
     */
    public String getOntologyName() {
        return ontologyName;
    }

    public String getOntologyIri() {
        return ontologyIri;
    }

    public Instant getDate() {
        return date;
    }

    public static class Builder<B extends FlatOntology.Builder<B, T>, T extends FlatOntology>
        extends EntityBuilder<Id, T> {
        protected String ontologyName;
        protected String ontologyIri;
        protected Instant date;

        public B ontologyName(String ontologyName) {
            this.ontologyName = ontologyName;
            return (B) this;
        }

        public B ontologyIri(String ontologyIri) {
            this.ontologyIri = ontologyIri;
            return (B) this;
        }

        public B date(Instant date) {
            this.date = date;
            return (B) this;
        }

        public T build() {
            return (T) new FlatOntology(this);
        }
    }
}
