/*
 * ==========================License-Start=============================
 * annotation-server : ImageResource
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.rest.resources;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.AnnotationCore;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.bgtask.BackgroundTask;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.Id;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.image.Image;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image.ImageStorage;
import de.uni_passau.fim.semantic_web.annotations.image.server.rest.beans.ErrorMessage;
import de.uni_passau.fim.semantic_web.annotations.image.server.rest.beans.annotations.AnnotationUpdateBean;
import de.uni_passau.fim.semantic_web.annotations.image.server.rest.beans.annotations.ImageUploadResponseBean;
import de.uni_passau.fim.semantic_web.annotations.image.server.rest.dto.annotations.AnnotationDTO;
import de.uni_passau.fim.semantic_web.annotations.image.server.rest.dto.bgtask.BackgroundTaskDTO;
import de.uni_passau.fim.semantic_web.annotations.image.server.rest.dto.image.AnnotatedImageDTO;
import de.uni_passau.fim.semantic_web.annotations.image.server.rest.dto.image.ImageDTO;
import de.uni_passau.fim.semantic_web.annotations.image.server.rest.dto.ui.PageCountDTO;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import javax.inject.Singleton;
import javax.print.attribute.standard.Media;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This resource is used to manage annotated images on the server. It supports:
 *
 * <ul>
 * <li>Uploading annotated images</li>
 * <li>Updating image data without touching annotations</li>
 * <li>Updating annotated images</li>
 * <li>Deleting annotated images</li>
 * <li>Retrieving thumbnails of images stored on this server</li>
 * </ul>
 */
@Singleton
@Path("/image")
public class ImageResource extends AbstractResource {
    private final static ObjectMapper MAPPER = new ObjectMapper();

    private final ImageStorage<Id> imageStorage;

    private final static int DEFAULT_PAGE = 1;
    private final static int DEFAULT_LIMIT = 50;

    public ImageResource(AnnotationCore core) {
        super(core);
        this.imageStorage = this.core.getImageStorage();
    }


    /**
     * Returns a list of all images that are currently stored on this server.
     *
     * @return {@link Response} containing a list of all images stored on this server.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getImageList(@QueryParam("limit") Integer limit, @QueryParam("page") Integer page) {
        if (limit == null) {
            limit = DEFAULT_LIMIT;
        }

        if (page == null) {
            page = DEFAULT_PAGE;
        }

        List<ImageDTO> res = imageStorage.getImages().skip(page - 1).limit(limit)
            .map(image -> new ImageDTO().fromConcrete(image))
            .collect(Collectors.toList());

        return Response.ok(res).build();
    }

    @GET
    @Path("/pagecount")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPageCount() {

        return Response.ok()
            .entity(new PageCountDTO((long)Math.ceil((double)imageStorage.getImages().count() / (double)DEFAULT_LIMIT)))
            .build();
    }

    /**
     * Gets a single image from the server.
     *
     * @param id ID of the image that is requested.
     * @return {@link Response} containing the requested image data.
     */
    @GET
    @Path("/{id}")
    @Produces({"image/png", "image/jpg", "application/json"})
    public Response getImageData(@Valid @NotNull @PathParam("id") String id) {
        final Id imageId = this.core.getIdFactory().createId(id);
        final byte[] imageData = imageStorage.getImage(imageId);
        if (imageData == null) {
            return Response
                .status(Response.Status.NOT_FOUND)
                .entity(new ErrorMessage("This image does not exist."))
                .build();
        }
        return Response
            .status(Response.Status.OK)
            .entity(imageData)
            .build();
    }

    /**
     * Gets the annotation data from a single image from the server.
     *
     * @param id ID of the image of which the annotation data is requested.
     * @return {@link Response} containing the requested annotation data.
     */
    @GET
    @Path("/{id}/annotations")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getImageJSON(@Valid @NotNull @PathParam("id") String id){
        log.debug("Requested image annotations by id: " + id);

        AnnotatedImageDTO res = new AnnotatedImageDTO()
            .fromConcrete(imageStorage.getImageAnnotations(core.getIdFactory().createId(id)));

        return Response
            .status(Response.Status.OK)
            .entity(res)
            .build();
    }

    /**
     * Gets the annotation data from a single image from the server.
     *
     * @param id ID of the image of which the annotation data is requested.
     * @return {@link Response} containing the requested annotation data.
     */
    @GET
    @Path("/{id}/annotations/relative")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getImageJSONRelative(@Valid @NotNull @PathParam("id") String id) {
        // TODO: FIX THIS

        return Response
            .status(Response.Status.NOT_FOUND)
            .build();
    }

    /**
     * Gets the thumbnail of an image from the server.
     *
     * @param id ID of the image of which the thumbnail is requested.
     * @return {@link Response} containing the requested thumbnail data.
     */
    @GET
    @Path("/{id}/thumbnail")
    @Produces({"image/png", "image/jpg"})
    public Response getImageThumbnail(@Valid @NotNull @PathParam("id") String id) {
        final Id imageId = this.core.getIdFactory().createId(id);
        log.debug(MARKER, "Requested image thumbnail by id: {}", imageId);
        return Response.ok(imageStorage.getImageThumbnail(imageId)).build();
    }

    /**
     * Updates the annotation data of a specified image.
     *
     * @param id ID of an {@link Image}
     *          where the annotations should get updated.
     * @param annotations New annotation data.
     * @return {@link Response} containing the updated image annotations.
     */

    @PUT
    @Path("/{id}/annotations")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateAnnotations(
        @Valid @NotNull @PathParam("id") String id,
        @Valid @NotNull AnnotationUpdateBean annotations) {
        final Id imageId = this.core.getIdFactory().createId(id);
        log.debug(MARKER, "Requested updating image annotations by id: {}", imageId.toString());

        if (annotations == null) {
            throw new IllegalArgumentException("Request must contain annotation object!");
        }

        imageStorage.updateImage(imageId, annotations.getAnnotations()
            .parallelStream()
            .map(AnnotationDTO::toConcrete)
            .collect(Collectors.toList()));

        return Response.ok(imageStorage.getImageAnnotations(imageId)).build();
    }

    /**
     * Uploads an image to the server.
     *
     * @param file           Image data to be uploaded.
     * @param fileDetail     Additional information about the file to be uploaded.
     * @param annotationJSON Annotations of the image to be uploaded, must be JSON.
     * @return {@link Response} containing the image id as it has been stored on the server.
     */
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public Response uploadImage(
        @Valid @NotNull @FormDataParam("file") FormDataBodyPart file,
        @Valid @NotNull @FormDataParam("file") FormDataContentDisposition fileDetail,
        @Valid @FormDataParam("annotations") FormDataBodyPart annotationJSON) {

        log.debug(MARKER, "User uploaded image and optional annotations with filename: '{}'", fileDetail.getFileName());

        final File imageFile = file.getValueAs(File.class);
        byte[] imageData;

        // Read incoming image data.
        try {
            imageData = Files.readAllBytes(imageFile.toPath());
        } catch (IOException e) {
            log.error(MARKER, "Could not read image file.", e);
            return Response
                .status(Response.Status.BAD_REQUEST)
                .entity(new ErrorMessage("Image file not valid."))
                .build();
        }

        // Handle attached annotation json.
        Id resultId;
        if (annotationJSON == null) {
            log.info(MARKER, "Trying to store image.");
            resultId = imageStorage.uploadImage(imageData, new LinkedList<>(), fileDetail.getFileName()).getId();
            log.info(MARKER, "Image successfully stored with id '{}'", resultId);
        } else {
            List<AnnotationDTO> annotations;
            try {
                annotations = MAPPER.readValue(
                    annotationJSON.getValue(),
                    new TypeReference<List<AnnotationDTO>>() {
                    });
            } catch (IOException e) {
                log.error(MARKER, "Could not parse annotations.", e);
                return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(new ErrorMessage("Annotations not valid."))
                    .build();
            }

            log.info(MARKER, "Trying to store image with attached annotations.");

            resultId = imageStorage.uploadImage(imageData,
                annotations
                    .parallelStream()
                    .map(AnnotationDTO::toConcrete)
                    .collect(Collectors.toList()), fileDetail.getFileName()).getId();

            log.info(MARKER, "Image and attached annotations successfully stored with id '{}'", resultId);
        }

        return Response
            .status(Response.Status.CREATED)
            .entity(new ImageUploadResponseBean(resultId))
            .build();
    }

    /**
     * Updates the image data of a specified image. Eventually existing annotations stay
     * untouched.
     *
     * @param file       New image file.
     * @param fileDetail Additional information about the new image file.
     * @param id         ID of the {@link de.uni_passau.fim.semantic_web.annotations.image.server.core
     *                   .entities.Image} that should get updated.
     * @return {@link Response} containing the previously uploaded image.
     */
    @POST
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateImageData(@Valid @NotNull @FormDataParam("image") FormDataBodyPart file,
                                    @Valid @NotNull @FormDataParam("image") FormDataContentDisposition fileDetail,
                                    @Valid @NotNull @PathParam("id") String id) {
        log.debug("Requested updating image data by id: {}" + id);

        File imageFile = file.getValueAs(File.class);
        byte[] image;

        // Read incoming image data.
        try {
            image = Files.readAllBytes(imageFile.toPath());
        } catch (IOException e) {
            log.error(MARKER, "Could not read bytes from received image file.", e);
            return Response
                .status(Response.Status.BAD_REQUEST)
                .entity(new ErrorMessage("Image file could not be read."))
                .build();
        }

        final Id imageId = this.core.getIdFactory().createId(id);

        log.info(MARKER, "Trying to update image data for id '{}'.", imageId.toString());
        imageStorage.updateImage(imageId, image);
        log.info(MARKER, "Updating image with id '{}' successful", imageId.toString());
        return Response.ok(imageStorage.getImage(imageId)).build();
    }

    /**
     * Deletes an image from the server.
     *
     * @param id ID of the {@link de.uni_passau.fim.semantic_web.annotations.image.server.core
     *           .entities.Image} to be deleted.
     * @return {@link Response} indicating whether the requested image has been successfully
     * deleted or not.
     */
    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteImage(@Valid @NotNull @PathParam("id") String id) {

        final Id imageId = this.core.getIdFactory().createId(id);


        log.debug(MARKER, "Requested deleting image by id: " + imageId.toString());
        try {
            log.info(MARKER, "Trying to delete image with id '{}'", imageId.toString());
            imageStorage.deleteImage(imageId);
        } catch (Exception e) {
            log.error(MARKER, "Could not delete image file.", e);
            return Response
                .status(Response.Status.BAD_REQUEST)
                .entity(new ErrorMessage("Image file could not be deleted."))
                .build();
        }
        return Response
            .status(Response.Status.ACCEPTED)
            .build();
    }

    /**
     * Starts a background task archiving all currently uploaded images on the server.
     *
     * @return  {@link Response} containing the ID of the initiated background task.
     */
    @GET
    @Path("/download")
    @Produces(MediaType.APPLICATION_JSON)
    public Response downloadImage() {
        BackgroundTask task = core.getBackgroundTaskManager().enqueue(() -> {
            // CALL DOWNLOAD FUNCTION HERE
        });

        return Response.ok().entity(new BackgroundTaskDTO().fromConcrete(task)).build();
    }
}
