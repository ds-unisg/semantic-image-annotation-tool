/*
 * ==========================License-Start=============================
 * annotation-server : FileImageDataStorage
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image.imgdata;

import de.uni_passau.fim.semantic_web.annotations.image.server.core.AnnotationCore;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.Id;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.image.Image;
import net.coobird.thumbnailator.Thumbnails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


/**
 * This class implements the {@link ImageDataStorage} interface and can be used to store uploaded
 * images in the file system.
 */
public class FileImageDataStorage extends ImageDataStorage {

    /**
     * Subfolder where thumbnails should get saved in.
     */
    private static final String THUMBNAIL_FOLDER_NAME = "thumbs";

    /**
     * Default resolution for the thumbnails to be generated.
     */
    private static final Integer[] THUMB_RES = {200, 200};

    private final String basePath;
    private final Logger log = LoggerFactory.getLogger(this.getClass().getName());

    /**
     * Creates a new instance.
     * <p>
     * Automatically creates folders for image data and thumbnails, if not yet present.
     */
    public FileImageDataStorage(AnnotationCore core) {
        super(core);

        basePath = core.getConfig().getString("fileStorage.path");
        log.debug(MARKER, "Base path for FileImageDataStorage: {}", basePath);

        // Create storage folder if it doesn't exist already
        final File storage = new File(basePath);

        if (!storage.exists() || !storage.isDirectory()) {
            log.info("Image storage folder does not exist, will create.");

            //noinspection ResultOfMethodCallIgnored
            storage.mkdir();
        }

        // Create thumbnail folder if it doesn't exist already
        final File thumbs = Paths.get(basePath, THUMBNAIL_FOLDER_NAME).toFile();
        if (!thumbs.exists() || !thumbs.isDirectory()) {
            log.info("Image thumbnail storage folder does not exist, will create at: {}", thumbs.toString());

            //noinspection ResultOfMethodCallIgnored
            thumbs.mkdir();
        }
    }

    /**
     * Stores a new image.
     *
     * @param img     Instance of {@link Image} the image data belongs to.
     * @param imgData Image data of the image to store in the file system.
     */
    @Override
    public synchronized void storeImage(Image img, byte[] imgData) {
        if (img.getId() == null) {
            throw new IllegalArgumentException("Image object does not have an ID yet!");
        }

        File target = Paths.get(basePath, img.getId().toString()).toFile();

        log.info("Will store image with id `{}` to file `{}`", img.getId(), target.toString());

        if (target.exists()) {
            log.error("Cannot store image that already exists.");
            throw new IllegalStateException("An image with this ID already exists."
                + " Use updateImage() instead.");
        }

        // Write image to disk
        try (FileOutputStream fos = new FileOutputStream(target)) {
            fos.write(imgData);
            log.info("Image '{}' successfully written to disk.", img.getId());
        } catch (FileNotFoundException e) {
            log.error("The image could not be written to disk, the file cannot be found: ", e);
        } catch (IOException e) {
            log.error("Another error occurred during the writing of the image file:", e);
        }

        // Write thumbnail to disk
        try (FileOutputStream fosThumb = new FileOutputStream(Paths.get(this.basePath, THUMBNAIL_FOLDER_NAME, img.getId().toString()).toFile())) {
            Thumbnails.of(target)
                .size(THUMB_RES[0], THUMB_RES[1])
                .outputFormat("jpg")
                .toOutputStream(fosThumb);
            log.info("Thumbnail for image '{}' successfully written to disk.", img.getId());
        } catch (IOException e) {
            log.error("Thumbnail for image '{}' could not be written to disk: {}", img.getId(), e.getMessage());
        }
    }

    /**
     * Retrieves the image data belonging to an {@link Image} from the file system.
     *
     * @param img {@link Image} object to retrieve the image data from.
     * @return Corresponding image data (binary).
     */
    @Override
    public byte[] getImage(Image img) {
        if (img == null || img.getId() == null) {
            log.warn(MARKER, "User trying to get image, but image is null.");
            return null;
        }

        try {
            final Path target = Paths.get(this.basePath, img.getId().toString());
            return Files.readAllBytes(target);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Retrieves the thumbnail image data belonging to an {@link Image} from the file system.
     *
     * @param img {@link Image} object to retrieve the image thumbnail data from.
     * @return Corresponding image thumbnail data (binary).
     */
    @Override
    public byte[] getThumbnail(Image img) {
        try {
            Path target = Paths.get(this.basePath, THUMBNAIL_FOLDER_NAME, img.getId().toString());
            return Files.readAllBytes(target);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Deletes the image data from an {@link Image} from the file system.
     *
     * @param img {@link Image} where the image data should get deleted from.
     */
    @Override
    public synchronized void deleteImage(Image img) {
        final Id id = img.getId();
        final Path targetPath = Paths.get(this.basePath, id.getId().toString());

        if (Files.exists(targetPath) && Files.isRegularFile(targetPath)) {
            try {
                Files.delete(targetPath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Updates the image data from an {@link Image}.
     *
     * @param img     {@link Image} of which the image data should get updated.
     * @param imgData New image data (binary).
     */
    @Override
    public synchronized void updateImage(Image img, byte[] imgData) {
        File target = Paths.get(this.basePath, img.getId().toString()).toFile();

        if (!target.exists() && !target.isFile()) {
            throw new UnsupportedOperationException("Can't update an image that does not exist!");
        } else {
            deleteImage(img);
            storeImage(img, imgData);
        }
    }
}
