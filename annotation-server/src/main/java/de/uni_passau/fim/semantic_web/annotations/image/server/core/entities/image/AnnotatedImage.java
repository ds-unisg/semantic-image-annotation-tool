package de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.image;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.annotation.Annotation;

import javax.validation.constraints.NotNull;
import java.util.List;

public class AnnotatedImage extends Image {
    @NotNull
    @JsonProperty("annotations")
    protected List<Annotation> annotations;

    private AnnotatedImage(AnnotatedImage.Builder builder) {
        super(builder);
        this.annotations = builder.annotations;
    }

    public List<Annotation> getAnnotations() {
        return annotations;
    }

    public void setAnnotations(List<Annotation> annotations) {
        this.annotations = annotations;
    }

    public static class Builder<B extends AnnotatedImage.Builder<B>>
        extends Image.Builder<B, AnnotatedImage> {
        private List<Annotation> annotations;

        public B annotations(List<Annotation> annotations) {
            this.annotations = annotations;
            return (B) this;
        }

        @Override
        public AnnotatedImage build() {
            return new AnnotatedImage(this);
        }
    }
}
