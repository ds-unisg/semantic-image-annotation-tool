package de.uni_passau.fim.semantic_web.annotations.image.server.rest.util;

import de.uni_passau.fim.semantic_web.annotations.image.server.core.AnnotationCore;
import de.uni_passau.fim.semantic_web.annotations.image.server.rest.AnnotationRESTServer;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

public class AnnotationRESTFeature {
    protected static final Marker MARKER = MarkerFactory.getMarker("restFeature");

    protected final AnnotationCore core;
    protected final AnnotationRESTServer server;

    public AnnotationRESTFeature(AnnotationCore core, AnnotationRESTServer server) {
        this.core = core;
        this.server = server;
    }
}
