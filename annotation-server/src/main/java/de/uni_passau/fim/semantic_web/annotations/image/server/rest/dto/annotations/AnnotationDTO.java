/*
 * ==========================License-Start=============================
 * annotation-server : AnnotationDTO
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.rest.dto.annotations;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.annotation.Annotation;
import de.uni_passau.fim.semantic_web.annotations.image.server.rest.dto.DTO;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")

@JsonSubTypes({
        @JsonSubTypes.Type(value = AnnotationCircleDTO.class, name = "circle"),
        @JsonSubTypes.Type(value = AnnotationPolygonDTO.class, name = "polygon"),
        @JsonSubTypes.Type(value = AnnotationRectangleDTO.class, name = "rect")
})

@JsonIgnoreProperties(ignoreUnknown = true)

public abstract class AnnotationDTO<T extends AnnotationDTO> extends DTO<Annotation, T> {

    @JsonInclude
    private String ontologyClassId;

    @JsonInclude
    private String ontologyClassName;

    @JsonInclude
    private String ontologyClassIri;

    @JsonInclude
    private String ontologyId;

    @JsonInclude
    private String fill;

    public AnnotationDTO() { }

    public String getOntologyClassId() {
        return ontologyClassId;
    }

    public void setOntologyClassId(String ontologyClassId) {
        this.ontologyClassId = ontologyClassId;
    }

    public String getOntologyClassName() {
        return ontologyClassName;
    }

    public void setOntologyClassName(String ontologyClassName) {
        this.ontologyClassName = ontologyClassName;
    }

    public String getOntologyClassIri() {
        return ontologyClassIri;
    }

    public void setOntologyClassIri(String ontologyClassIri) {
        this.ontologyClassIri = ontologyClassIri;
    }

    public String getOntologyId() {
        return ontologyId;
    }

    public void setOntologyId(String ontologyId) {
        this.ontologyId = ontologyId;
    }

    public String getFill() {
        return fill;
    }

    public void setFill(String fill) {
        this.fill = fill;
    }

    @Override
    public abstract Annotation toConcrete();

    @Override
    public abstract T fromConcrete(Annotation concrete);
}
