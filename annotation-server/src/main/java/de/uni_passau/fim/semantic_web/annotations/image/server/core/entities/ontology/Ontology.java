/*
 * ==========================License-Start=============================
 * annotation-server : Ontology
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.ontology;

import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.Id;

import java.util.*;

/**
 * This class defines an ontology that is stored on the server.
 */
public class Ontology extends FlatOntology {
    private SortedSet<OntologyClass> ontologyChildren;

    /**
     * Creates a new ontology that is unnamed and has no classes.
     */
    private Ontology(Ontology.Builder builder) {
        super(builder);
        this.ontologyChildren = Optional.ofNullable(builder.ontologyChildren).orElse(new TreeSet<>());
    }

    /**
     * Returns the root {@link OntologyClass}es of this {@link Ontology}.
     *
     * @return Root elements of this {@link Ontology}. Note that this set may not be modified.
     */
    public SortedSet<OntologyClass> getOntologyChildren() {
        return Collections.unmodifiableSortedSet(ontologyChildren);
    }

    public void setOntologyChildren(SortedSet<OntologyClass> ontologyChildren) {
        this.ontologyChildren = ontologyChildren;
    }

    /**
     * Adds a new root class to this {@link Ontology}.
     *
     * @param oc Root class to be added.
     */
    public void addOntologyChildClass(OntologyClass oc) {
        ontologyChildren.add(oc);
    }

    /**
     * Removes a root class from this {@link Ontology}.
     *
     * @param oc Root class to be removed.
     * @return {@code True} if the class has been removed successfully, {@code False} if not.
     */
    public boolean removeOntologyChildClass(OntologyClass oc) {
        return ontologyChildren.remove(oc);
    }

    public Optional<OntologyClass> findClassByName(String className) {
        List<OntologyClass> classPool = traverse(this);
        return classPool.stream().filter(clazz ->
                clazz.getClassName().equals(className)).findFirst();
    }

    public Optional<OntologyClass> findClassById(Id id) {
        List<OntologyClass> classPool = traverse(this);
        return classPool
            .stream()
            .filter(clazz -> clazz.getId().equals(id))
            .findFirst();
    }

    private static List<OntologyClass> traverse(Ontology ont) {
        List<OntologyClass> classes = new LinkedList<>();
        ont.getOntologyChildren().forEach(child -> traverse(child, classes));
        return classes;
    }

    private static void traverse(OntologyClass current, List<OntologyClass> visited) {
        if (!visited.contains(current)) {
            visited.add(current);
            current.getSubClasses().forEach(subclass -> traverse(subclass, visited));
        }
    }

    public static class Builder<B extends Ontology.Builder<B>>
        extends FlatOntology.Builder<B, Ontology> {

        protected SortedSet<OntologyClass> ontologyChildren;

        public B ontologyChildren(SortedSet<OntologyClass> ontologyChildren) {
            this.ontologyChildren = ontologyChildren;
            return (B) this;
        }

        @Override
        public Ontology build() {
            return new Ontology(this);
        }
    }
}
