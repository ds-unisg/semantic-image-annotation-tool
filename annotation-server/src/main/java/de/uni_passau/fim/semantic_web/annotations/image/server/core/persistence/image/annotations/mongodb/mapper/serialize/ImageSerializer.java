/*
 * ==========================License-Start=============================
 * annotation-server : ImageSerializer
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image.annotations.mongodb.mapper.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.AnnotationCore;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.image.Image;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image.annotations.mongodb.mapper.AnnotationMongoModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;
import java.util.Optional;

public abstract class ImageSerializer<T extends Image> extends JsonSerializer<T> {

    protected static final Marker MARKER = MarkerFactory.getMarker("json");

    protected final Logger log = LoggerFactory.getLogger(ImageSerializer.class);

    protected final AnnotationCore core;
    protected ObjectMapper mapper;

    public ImageSerializer(AnnotationCore core) {
        this.core = core;
    }

    protected ObjectMapper getMapper() {
        if (mapper == null) {
            mapper = new ObjectMapper();
            mapper.registerModule(new AnnotationMongoModule(core));
        }
        return mapper;
    }

    @Override
    public void serialize(T image, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
        throws IOException {

        Optional.ofNullable(image.getId()).orElseThrow(() -> new IllegalStateException("The id of this Image is null"));

        jsonGenerator.writeStartObject();
        jsonGenerator.writeObjectField("_id", image.getId().getId());
        jsonGenerator.writeObjectField("size", image.getSize());
        jsonGenerator.writeStringField("filename", image.getFilename());

        //jsonGenerator.writeEndObject();
    }
}
