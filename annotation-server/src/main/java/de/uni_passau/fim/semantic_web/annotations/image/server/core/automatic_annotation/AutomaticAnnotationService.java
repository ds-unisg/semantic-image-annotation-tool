/*
 * ==========================License-Start=============================
 * annotation-server : AutomaticAnnotationService
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.core.automatic_annotation;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.typesafe.config.Config;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.AnnotationCore;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.automatic_annotation.model.*;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.image.AnnotatedImage;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.annotation.Annotation;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.annotation.shapes.AnnotationRectangle;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.exceptions.UnsuccessfulExecutionException;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 *
 */
public class AutomaticAnnotationService {

    private final static ObjectMapper JSON_OBJECT_MAPPER = new ObjectMapper();
    private final static Marker MARKER = MarkerFactory.getMarker("core");

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final AnnotationCore core;

    private final HttpUrl remoteApiVersion;
    private final HttpUrl remoteApiAvailableModelTypes;
    private final HttpUrl remoteApiAvailableModelsForType;
    private final HttpUrl remoteApiClassification;

    private final OkHttpClient client;

    public AutomaticAnnotationService(AnnotationCore core) {
        this.core = core;

        log.info(MARKER, "Will initialize Cache directory at: {}", core.getConfig().getString("fileStorage.cache"));
        final Cache cache = new Cache(
            Paths.get(core.getConfig().getString("fileStorage.cache")).toFile(),
            10 * 1024 * 1024);

        final Config apiConfig = core.getConfig().getConfig("automaticAnnotationAPI");

        log.debug("Will initialize HTTPClient with authorization header.");
        this.client = new OkHttpClient.Builder()
            .cache(cache)
            .addInterceptor(chain ->
                chain.proceed(chain
                    .request()
                    .newBuilder()
                    .header(
                        "Authorization",
                        Credentials.basic(apiConfig.getString("username"), apiConfig.getString("password")))
                    .build())
            )
            .build();

        HttpUrl remoteApiBaseURL = HttpUrl.parse(apiConfig.getString("urls.baseUrl"));

        if (remoteApiBaseURL == null) {
            throw new IllegalArgumentException("The provided urls.baseUrl resolved to null.");
        }
        this.remoteApiVersion = remoteApiBaseURL
            .newBuilder()
            .addPathSegments(apiConfig.getString("urls.version"))
            .build();
        this.remoteApiAvailableModelTypes = remoteApiBaseURL
            .newBuilder()
            .addPathSegments(apiConfig.getString("urls.availableModelTypes"))
            .build();
        this.remoteApiAvailableModelsForType = remoteApiBaseURL
            .newBuilder()
            .addPathSegments(apiConfig.getString("urls.availableModelsForType"))
            .build();
        this.remoteApiClassification = remoteApiBaseURL
            .newBuilder()
            .addPathSegments(apiConfig.getString("urls.classification"))
            .build();
    }

    /**
     * Returns the currently running instance of the remote automatic annotation and classification service.
     *
     * @return currently running remote version
     */
    public AutomaticAnnotationVersion getVersionInfo() {

        log.debug(MARKER, "Received request to get version info.");

        Request request = new Request.Builder()
                .url(this.remoteApiVersion)
                .build();

        try {
            okhttp3.Response response = client.newCall(request).execute();

            if (response.isSuccessful()) {
                log.info(MARKER, "Received response for version request.");
                String responseString = Objects.requireNonNull(response.body()).string();
                return JSON_OBJECT_MAPPER.readValue(responseString, AutomaticAnnotationVersion.class);
            } else {
                log.error(MARKER, "Request was sent but response not received: {} → {}", response.code(), response.message());
                throw new UnsuccessfulExecutionException("Request/Response to automatic annotation services was unsuccessful " + response.code());
            }
        } catch (IOException e) {
            log.error(MARKER, "The response could not be loaded.");
            throw new UnsuccessfulExecutionException("Request/Response to automatic annotation services was unsuccessful.");
        }
    }

    /**
     * Returns the available model types.
     *
     * @return available model types
     */
    public AutomaticAnnotationAvailableModelTypes getAvailableModelTypes() {

        log.debug(MARKER, "Received request to get available model types.");

        Request request = new Request.Builder()
                .url(this.remoteApiAvailableModelTypes)
                .build();

        try {
            okhttp3.Response response = client.newCall(request).execute();

            if (response.isSuccessful()) {
                log.info(MARKER, "Received response for available model types request.");
                String responseString = Objects.requireNonNull(response.body()).string();
                return JSON_OBJECT_MAPPER.readValue(responseString, AutomaticAnnotationAvailableModelTypes.class);
            } else {
                log.error(MARKER, "Request was sent but response not received: {} → {}", response.code(), response.message());
                throw new UnsuccessfulExecutionException("Request/Response to automatic annotation services was unsuccessful " + response.code());
            }
        } catch (IOException e) {
            log.error(MARKER, "The response could not be loaded.");
            throw new UnsuccessfulExecutionException("Request/Response to automatic annotation services was unsuccessful.");
        }
    }

    /**
     * Returns the available models for a specific type.
     *
     * @return available model types
     */
    public AutomaticAnnotationAvailableModelsForType getAvailableModelsForType(String type) {

        log.debug(MARKER, "Received request to get available model for a specific type: " + type);

        if (type == null) {
            throw new IllegalArgumentException("`type` must not be null");
        }

        Request request = new Request.Builder()
                .url(this.remoteApiAvailableModelsForType
                        .newBuilder()
                        .addPathSegment(type)
                        .build())
                .build();

        try {
            okhttp3.Response response = client.newCall(request).execute();

            if (response.isSuccessful()) {
                log.info(MARKER, "Received response for available models for type '" + type + "' request.");
                String responseString = Objects.requireNonNull(response.body()).string();
                response.close();
                return JSON_OBJECT_MAPPER.readValue(responseString, AutomaticAnnotationAvailableModelsForType.class);
            } else {
                response.close();
                log.error(MARKER, "Request was sent but response not received: {} → {}", response.code(), response.message());
                throw new UnsuccessfulExecutionException("Request/Response to automatic annotation services was unsuccessful " + response.code());
            }

        } catch (IOException e) {
            log.error(MARKER, "The response could not be loaded.");
            throw new UnsuccessfulExecutionException("Request/Response to automatic annotation services was unsuccessful.");
        }
    }

    public List<Annotation> getAutomaticAnnotationForImage(@Nonnull AnnotatedImage image, @Nonnull AutomaticAnnotationModel model) {
        Objects.requireNonNull(image, "image must not be null");
        Objects.requireNonNull(model, "model must not be null");

        log.debug(MARKER, "Received request to get annotations for image with id '{}' with model '{}'", image.getId(), model.getModelId());
        if (!image.getAnnotations().isEmpty()) {
            log.info(MARKER, "The given image already has annotations attached.");
            return image.getAnnotations();
        }

        // load image
        byte[] imageData = this.core.getImageStorage().getImage(image.getId());

        MultipartBody body = new MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart("images[]", image.getId().getId().toString(), RequestBody.create(MediaType.parse("image/jpeg"), imageData))
            .build();

        Request request = new Request.Builder()
            .url(this.remoteApiClassification
                .newBuilder()
                .addPathSegment(model.getType())
                .addPathSegment(model.getModelId())
                .build())
            .post(body)
            .build();

        Optional<Response> response;
        try {
            response = Optional.of(client.newCall(request).execute());
        } catch (IOException e) {
            log.error(MARKER, "Could not execute request.");
            throw new UnsuccessfulExecutionException(e.getMessage());
        }

        if (response.isPresent()) {
            Response r = response.get();

            if (r.isSuccessful()) {

                try {
                    String responseString = Objects.requireNonNull(r.body()).string();
                    AutomaticAnnotations annotations = JSON_OBJECT_MAPPER.readValue(responseString, AutomaticAnnotations.class);
                    return annotations
                        .getAnnotations()
                        .parallelStream()
                        .map(aa -> {
                            int top = 0;
                            int left = 0;
                            int width = image.getSize().getWidth();
                            int height = image.getSize().getHeight();
                            double scaleX = 1;
                            double scaleY = 1;
                            String fill = "(255,255,255,255)";
                            return new AnnotationRectangle.Builder()
                                .top(top).left(left)
                                .width(width).height(height)
                                .scaleX(scaleX).scaleY(scaleY)
                                .fill(fill)
                                .build();
                        })
                        .collect(Collectors.toList());
                } catch (IOException e) {
                    log.error(MARKER, "Could not convert received annotations to JSON.");
                }


            } else {
                log.error(MARKER, "Response was received but not successful.");
            }
            r.close();

        } else {
            log.error(MARKER, "Response was null.");
        }

        return null;
    }
}
