# Annotation Tool

This package contains two projects:

- `annotation-server`: Backend
- `annotation-web`: Frontend

Look at their respective README.md on how to run them individually, or use `docker-compose` to wrap them in Docker.
