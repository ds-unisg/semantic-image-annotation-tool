'use strict';

/**
 * @ngdoc function
 * @name AnnotationToolApp.controller:ApplicationCtrl
 * @description
 * # ApplicationCtrl
 * Controller of the AnnotationToolApp
 */
angular.module('AnnotationToolApp').controller('AnnotationsCtrl', [
	'Configuration',
	'$scope',
	'$rootScope',
	'$routeParams',
	'$location',
	'Notification',
	'FileUploader',
	'AnnotationFactory',
	'ClassificationFactory',
	'OntologyFactory',
	'$window',
	'ModalService',
	function (Configuration, $scope, $rootScope, $routeParams, $location, Notification, FileUploader, AnnotationFactory, ClassificationFactory, OntologyFactory, $window, ModalService) {
		let self = this;

		this.annotations = [];

		// Result object containing 'success' boolean key determining whether the canvas has loaded successfully external data.
		this.canvasImportResult = null;

		// Selected fabricJS object.
		this.selectedObject = null;

		// Selected Ontology Element in OntologyViewer, wrapper object containing ontology id/classid/iri/classname.
		this.selectedClass = null;

		this.isRemoteImage = $routeParams.id !== undefined;
		this.showImage = false;
		this.imageInput = false;
		this.changed = false;

		if (this.isRemoteImage) {
			AnnotationFactory
				.getAnnotations({id: $routeParams.id})
				.$promise
				.then(
					success => {
						self.imageInput = Configuration.image + '/' + $routeParams.id;
						self.annotations = success.annotations;
						self.showImage = true;
					},
					error => {
						Notification.success('Failed to fetch annotations from server!');
					}
				)
				.finally(() => {

				});
		} else {
			this.annotations = [];
		}

		/*
		* BASIC IMAGE ACTIONS
		*/
		this.reset = function () {
			this.selectedClass = null;
			this.showImage = false;
			delete this.imageInput;
			this.showProgress = false;

			this.annotations = {};
			this.predictions = {};

			this.changed = false;
			uploader.clearQueue();

			$('input').val(null);
		};

		this.uploadImage = function () {
			if (!self.imageInput) {
				Notification.error('Upload failed: No image loaded.');
			} else {
				uploader.uploadAll();
			}
		};

		this.updateImage = function () {
			let self = this;

			AnnotationFactory
				.updateAnnotations({id: $routeParams.id}, {annotations: self.annotations})
				.$promise
				.then(
					success => {
						Notification.success('Image updated successfully.');
					},
					error => {
						console.log(error);
						Notification.error('Failed to update image!');
					}
				)
				.finally(() => {
					self.changed = false;
				});
		};

		this.delete = function () {
			if ($routeParams.id === undefined) {
				let notUploadedMsg = 'Can\'t delete an image that is not uploaded to the server.';
				console.error(notUploadedMsg);
				Notification.error(notUploadedMsg);
			} else {

				AnnotationFactory
					.deleteImage({id: $routeParams.id})
					.$promise
					.then(
						success => {
							Notification.success('Image successfully deleted from server!');
							$location.path('/browse');
						},
						error => {
							console.log('Deleting image failed with', error);
							Notification.error('Failed to delete image from server: ' + error.code);
						}
					)
					.finally(() => {

					});
			}
		};

		/*
		* FILE LOADING & ANNOTATION UPLOADING
		*/
		const uploader = $scope.uploader = new FileUploader({
			url: Configuration.image,
			removeAfterUpload: false
		});

		uploader.onAfterAddingFile = function (fileItem) {
			if (uploader.getNotUploadedItems().length > 1) {
				uploader.removeFromQueue(0);
			}

			let fr = new FileReader();
			fr.onload = function (loadEvt) {
				$scope.$apply(function () {
					self.imageInput = null;
					self.imageInput = loadEvt.target.result;
					self.showImage = true;
				})
			};
			fr.readAsDataURL(fileItem._file);
		};

		uploader.onErrorItem = function (item, response, status, headers) {
			Notification.error('Failed to upload image!');
			console.error(response);
		};

		uploader.onBeforeUploadItem = function (fileItem) {
			fileItem.formData.push({annotations: JSON.stringify(self.annotations)});
		};

		uploader.onCompleteItem = function (item, response, status, headers) {
			self.changed = false;

			Notification.success('Image successfully uploaded!');

			if (!self.isRemoteImage) {
				$location.path('/annotations/' + response.id);
			}
		};

		uploader.onCompleteAll = function () {
			// Mark image as not uploaded again
			uploader.queue[0].isUploaded = false;
			uploader.queue[0].isReady = true;
		};

		/*
		* USER EVENT LISTENERS
		*/
		// Watch for object selections on the canvas.
		$scope.$watch('annotationsCtrl.selectedClass', function (newVal, oldVal) {
			// Forbid if no object is selected.
			if (!self.selectedObject) {
				self.selectedClass = null;
			}

			if (newVal && self.selectedObject) {
				self.selectedObject.ontologyId = self.selectedClass.ontologyId;
				self.selectedObject.ontologyClassId = self.selectedClass.ontologyClassId;

				// Remember this change.
				self.changed = true;
			}
		});

		// Watch for ontology class selections inside the ontology viewer.
		$scope.$watch('annotationsCtrl.selectedObject', function (newVal, oldVal) {
			// Discard any currently selected class.
			self.selectedClass = null;

			if (!newVal) {
				return;
			}

			let targetClass = newVal.ontologyClassId;
			let targetOntology = newVal.OntologyId;

			if (!targetClass || !targetOntology) {
				console.log(targetClass)
				console.log(targetOntology)
				return;
			}

			OntologyFactory
				.getOntology({id: targetOntology})
				.$promise
				.then(
					success => {
						const ontologyChildren = success.ontologyChildren;

						function bfs(node, targetId) {
							let res = null;

							if (node === null || node.subClasses === null || node.subClasses === undefined) {
								return null;
							}

							if (node.id === targetId) {
								return node;
							}

							node.subClasses.every(function (ontologyClass) {
								if (ontologyClass.id === targetId) {
									res = ontologyClass;
									return false;
								} else {
									// Step down in recursion
									res = bfs(ontologyClass, targetId);

									return res === null;
								}
							});

							return res;
						}

						let result = null;
						ontologyChildren.every(function (child, idx) {
							result = bfs(child, targetClass);

							return !result;
						});

						if (result) {
							self.selectedClass = {
								'ontologyId': targetOntology,
								'ontologyClassId': result.id,
								'ontologyClassName': result.className,
								'ontologyClassIri' : result.classIri
							}
						}
					},
					error => {
						Notification.error('Could not fetch ontology: ' + error.code);
						console.error(error);
					}
				)
				.finally(() => {

				});
		});

		// Watch changes on the annotations collection (e.g. size increase/decrease).
		$scope.$watchCollection('annotationsCtrl.annotations', function(newVal, oldVal) {
			if (self.showImage && self.canvasImportResult && self.canvasImportResult.success) {
				self.changed = true;
			}
		})

		/*
		* HANDLING PAGE EXIT MESSAGES
		*/
		const beforeunload = function(event) {
			event.preventDefault();
			event.returnValue = "...";
		};

		$window.addEventListener('beforeunload', beforeunload);

		$scope.$on('$destroy', function() {
			$window.removeEventListener('beforeunload', beforeunload);
		});

		const destroyLocationChangeListener = $rootScope.$on('$locationChangeStart', function(event, next, current) {
			if (self.changed) {
				event.preventDefault();

				ModalService.showModal({
					templateUrl: 'views/templates/modal/yesnomodal.tpl.html',
					controller: 'YesNoModal',
					inputs: {
						title: "WARNING",
						text: "You still have unsaved changes left. Do you really want to leave now?",
						yesText: "YES",
						noText: "NO",
						yesButtonClass: "btn-danger",
						noButtonClass: "btn-primary"
					}
				}).then(function(modal) {
					modal.element.modal();
					modal.close.then(function(result) {
						if (result) {
							destroyLocationChangeListener();
							$location.path(next);
						}
					})
				});
			}
		})
	}
]);
