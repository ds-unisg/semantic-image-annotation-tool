'use strict';

/**
 * @ngdoc function
 * @name AnnotationToolApp.controller:NavigationCtrl
 * @description
 * # NavigationCtrl
 * Controller of the AnnotationToolApp
 */
angular.module('AnnotationToolApp')
	.controller('NavigationCtrl', ['$location', function ($location) {
		var self = this;

		this.navCollapsed = true;

		this.navToggle = function () {
			self.navCollapsed = !self.navCollapsed;
		};

		this.closeNav = function () {
			if (!self.navCollapsed) {
				self.navCollapsed = true;
			}
		};

		this.isCurrentPage = function (path) {
			return $location.path() === path;
		};
	}]);
