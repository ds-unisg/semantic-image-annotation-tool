'use strict';

/**
 * @ngdoc service
 * @name AnnotationToolApp.AuthenticationInterceptor
 * @description
 * # AuthenticationInterceptor
 * Factory in the AnnotationToolApp.
 */
angular.module('AnnotationToolApp')
	.factory('AuthenticationInterceptor', [
		'Configuration',
		function (Configuration) {

			function generateAuth(username, password) {
				return 'Basic ' + btoa(username + ':' + password);
			}

			return {
				request: function (config) {

					config.headers = config.headers || {};
					if (config.url) {

						if (Configuration.authRequired && config.url.indexOf(Configuration.baseUrl) === 0) {
							config.headers.Authorization = generateAuth(Configuration.username, Configuration.password);
						}

					}

					return config;
				}
			};
		}]);
