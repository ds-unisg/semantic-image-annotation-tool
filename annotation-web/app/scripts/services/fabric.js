'use strict';

/**
 * @ngdoc service
 * @name AnnotationToolApp.Fabric
 * @description
 * # Fabric
 * Constant in the AnnotationToolApp.
 */
angular.module('AnnotationToolApp')
	.constant('fabric', window.fabric);

fabric.Object.prototype.toObject = (function (toObject) {
	return function (properties) {
		return fabric.util.object.extend(toObject.call(this, properties), {
			ontologyClassId: this.ontologyClassId,
			ontologyId: this.ontologyId
		});
	};
})(fabric.Object.prototype.toObject);
