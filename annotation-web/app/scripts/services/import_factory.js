'use strict';

/**
 * @ngdoc service
 * @name AnnotationToolApp.ImportFactory
 * @description
 * # ImportFactory
 * Factory in the AnnotationToolApp.
 */
angular.module('AnnotationToolApp')
	.factory('ImportFactory', ($resource, Configuration) => {

		return $resource(
			Configuration.import,
			{}, // no default parameters
			{
				getAvailableImporters: {
					method: 'GET',
					isArray: true
				}
			}
		);


	});
