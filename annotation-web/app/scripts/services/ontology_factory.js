'use strict';

/**
 * @ngdoc service
 * @name AnnotationToolApp.OntologyFactory
 * @description
 * # OntologyFactory
 * Factory in the AnnotationToolApp.
 */
angular.module('AnnotationToolApp')
	.factory('OntologyFactory', ($resource, Configuration) => {

		return $resource(
			Configuration.ontology,
			{}, // no default parameters
			{
				getAllOntologies: {
					method: 'GET',
					isArray: true
				},

				getOntology: {
					method: 'GET',
					url: Configuration.ontology + '/:id'
				},

				putOntology: {
					method: 'PUT'
				},

				deleteOntology: {
					method: 'DELETE',
					url: Configuration.ontology + '/:id'
				}
			}
		);


	});
