'use strict';

/**
 * @ngdoc filter
 * @name AnnotationToolApp.filter:ontologyelementinfo
 * @function
 * @description
 * # ontologyelementinfo
 * Filter in the AnnotationToolApp.
 */
angular.module('AnnotationToolApp')
	.filter('ontologyelementinfo', function () {
		return function (input) {
			let html = [];

			let keys = Object.keys(input);
			keys.forEach(function (key) {
				let value = input[key];
				html.push(key + ': ' + value);
			});

			return html.join(', ');
		};
	});
