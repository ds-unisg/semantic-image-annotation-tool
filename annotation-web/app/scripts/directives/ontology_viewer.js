'use strict';

/**
 * @ngdoc directive
 * @name AnnotationToolApp.directive:ontologyViewer
 * @description
 * # ontologyViewer
 */
angular.module('AnnotationToolApp')
	.directive('ontologyViewer', ['OntologyFactory', '$locale', function (OntologyFactory, $locale) {
		return {
			templateUrl: 'views/templates/ontology_viewer.tpl.html',
			restrict: 'E',
			scope: {
				selectedElement: '='
			},
			link: function postLink(scope, element, attrs) {
				scope.availableOntologies = null;
				scope.selectedOntology = null;
				scope.selectedElement = null;

				scope.ontology = null;
				scope.filteredOntology = null;

				scope.searchText = {};
				scope.searchText.text = '';

				// Languages
				scope.availableLangauges = ['de', 'en', 'fr'];
				scope.selectedLanguage = {
					language: $locale.localeID.slice(0, 2)
				};

				scope.select = function (ontologyElementData) {
					scope.selectedElement = {
						'ontologyId': scope.selectedOntology.id,
						'ontologyClassId': ontologyElementData.id,
						'ontologyClassIri': ontologyElementData.classIri,
						'ontologyClassName': ontologyElementData.className
					};
				};

				scope.expandOntology = function() {
					scope.$broadcast('ontologyViewer:expand');
				}

				OntologyFactory
					.getAllOntologies()
					.$promise
					.then(
						success => {
							scope.availableOntologies = success;

							// Default to first available ontology
							if (scope.availableOntologies.length > 0) {
								scope.selectedOntology = scope.availableOntologies[0];
								scope.changeOntology();
							}
						},
						error => {
							console.log(error);
						})
					.finally(() => {
					});


				// Ontology changing
				scope.changeOntology = function () {
					OntologyFactory
						.getOntology({id: scope.selectedOntology.id})
						.$promise
						.then(
							success => {
								scope.ontology = success;
								scope.filteredOntology = success;
							},
							error => {
								console.log(error);
								Notification.error('Could not get this specific ontology: ' + error.code);
							}
						)
						.finally(() => {

						});
				};

				// Features regarding ontology search.
				String.prototype.insensitiveEquals = function(cmp) {
					let self = this.toUpperCase();
					let other = cmp.toUpperCase();

					return self == other;
				}

				scope.filterOntology = function () {
					let searchQuery = scope.searchText.text;

					if (searchQuery && $.trim(searchQuery).length > 1) {
						let output = JSON.parse(JSON.stringify(scope.ontology, (key, value) => {
							if (key === 'ontologyChildren') {
								return undefined
							} else {
								return value;
							}
						}));

						output.ontologyChildren = [];
						
						scope.ontology.ontologyChildren.forEach(function(child) {
							let searchResult = processElement(child, searchQuery);

							if (searchResult) {
								output.ontologyChildren.push(searchResult);
							}
						});

						scope.filteredOntology = output;
					} else {
						scope.filteredOntology = scope.ontology;
					}
				};


				function processElement(element, query) {
					// Create copy of element (excluding subclass objects).
					let elementCopy = JSON.parse(JSON.stringify(element, (key, value) => {
						if (key === 'subClasses') {
							return undefined;
						} else {
							return value;
						}
					}));

					elementCopy.subClasses = [];

					// Walk through each subclass of element.
					element.subClasses.forEach(function (subClass) {
						let searchResult = processElement(subClass, query);
						
						if (searchResult) {
							elementCopy.subClasses.push(searchResult);
						}
					});

					// Validate against search query.
					if (checkQuery(element, query) || elementCopy.subClasses.length > 0) {
						return elementCopy;
					} else {
						return null;
					}
				}

				/**
				 * Checks if the query matches a literal or a comment in the given element.
				 * @param element
				 * @param query
				 * @returns {boolean}
				 */
				function checkQuery(element, query) {
					let matches = false;
					let search = scope.searchModel;

					// Check class name
					if (element.className.match(new RegExp(query, "i"))) {
						matches = true;
					}
					
					// Check literals
					Object.keys(element.literals).every(function(language) {
						if (element.literals[language].match(new RegExp(query, "i"))) {
							matches = true;
							return false;
						} else {
							return true;
						}
					});

					// Check comments
					Object.keys(element.comments).every(function(language) {
						if (element.comments[language].match(new RegExp(query, "i"))) {
							matches = true;
							return false;
						} else {
							return true;
						}
					});

					return matches;
				}
			}
		};
	}]);
