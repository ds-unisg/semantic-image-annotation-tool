'use strict';

/**
 * @ngdoc directive
 * @name AnnotationToolApp.directive:batchImageUploader
 * @description
 * # batchImageUploader
 */
angular.module('AnnotationToolApp')
	.directive('batchImageUploader', ['Configuration', 'FileUploader', function (Configuration, FileUploader) {
		return {
			template: `

    <div class="card">
      <div class="card-header">Batch image upload</div>
      <div class="card-body">

      <div ng-if="uploader" class="uploader" nv-file-drop uploader="uploader">
      Drop your files here...
      </div>

      <div ng-if="uploader.queue.length >= 1">
      <table class="table">
      <thead>
        <tr>
          <th>Thumbnail</th>
          <th>Filename</th>
          <th>Progress</th>
          <th>Uploaded</th> 
        </tr>
      </thead>
      <tbody>
        <tr ng-repeat="item in uploader.queue">
          <td><div upload-preview="{file: item._file}"></div></td>
          <td>{{item.file.name}}</td>
          <td>{{item.progress}}%</td>
          <td>{{item.isUploaded}}</td>
        </tr>
      </tbody>
    </table>

    <button type="button" class="btn btn-primary" ng-click="uploader.uploadAll()">Upload</button>
    <button type="button" class="btn btn-danger" ng-click="uploader.clearQueue()">Clear Queue</button>
    
        </div>      
      </div>
    </div>
    `,
			restrict: 'E',
			scope: {
				something: '='
			},
			link: function (scope, element, attrs) {
				scope.uploader = new FileUploader({
					url: Configuration.image,
					removeAfterUpload: true
				});

				scope.uploader.onBeforeUploadItem = function (item) {
					item.formData.push({annotations: JSON.stringify({annotations: []})});
				}
			}
		};
	}]);
