'use strict';

/**
 * @ngdoc directive
 * @name AnnotationToolApp.directive:serverInfo
 * @description
 * # serverInfo
 */
angular.module('AnnotationToolApp')
	.directive('serverInfo', ['ServerInfoFactory', '$timeout', function (ServerInfoFactory, $timeout) {
		return {
			templateUrl: 'views/templates/server_info.tpl.html',
			restrict: 'E',
			scope: {
				serverInfo: '='
			},
			link: function (scope) {
				let timeout;

				function getServerInfo() {
					scope.serverInfoLoading = true;

					ServerInfoFactory
						.getServerInfo()
						.$promise
						.then(
							data => {

								if (data) {
									scope.serverInfo = data;
									scope.percentFree = data.freeMemory / data.maxMemory;
									scope.percentAllocated = data.allocatedMemory / data.maxMemory - scope.percentFree;
									scope.percentMax = 1 - scope.percentAllocated - scope.percentFree;
								}

								timeout = $timeout(getServerInfo, 1500);
							},
							failure => {
								console.log(failure);
							}
						)
						.finally(() => {
							scope.serverInfoLoading = false;
						});
				}

				getServerInfo();

				// Stop fetching system info data when directive is destroyed.
				scope.$on('$destroy', () => {
					$timeout.cancel(timeout);
				});
			}
		};
	}]);
