'use strict';

/**
 * @ngdoc directive
 * @name AnnotationToolApp.directive:annotateImage
 * @description This directive provides a canvas for annotating input images using various shapes.
 */
angular.module('AnnotationToolApp')
	.directive('annotateImage', ['fabric', 'ModalService', function (fabric, ModalService) {
		return {
			restrict: 'E',
			scope: {
				annotations: '=',
				selectedObject: '=',
				image: '=',
				importResult: '='
			},
			templateUrl: 'views/templates/annotations.tpl.html',
			link: function (scope, element) {
				let self = this;

				let image = new Image();
				let initialDataLoaded = false;

				let viewport = new fabric.Canvas('fabricCanvas', {
					selection: false, preserveObjectStacking: true,
					imageSmoothingEnabled: false
				});

				scope.currentTool = null;
				let minScaleFactor = null;

				let undoStack = [];
				let redoStack = [];

				/**
				 * DIRECTIVE CONFIGURATION
				 */
				let polygonLineWidth = 1;
				let polygonCircleSize = 5;

				let edgeThresholdPixels = 75;
				let autoScrollSpeedCapPixels = 40;

				/**
				 * BASIC CANVAS OPERATIONS
				 */
				scope.setTool = function (selectedTool) {
					if (scope.currentTool) {
						scope.currentTool.reset();
					}

					if (selectedTool && selectedTool.handleSelection) {
						selectedTool.handleSelection();
						scope.currentTool = selectedTool;
					}
				}

				scope.resetTool = function () {
					if (scope.currentTool) {
						scope.currentTool.reset();
					}
				};

				scope.clear = function (showModal) {
					const clearFunc = function() {
						viewport.clear();
						viewport.setBackgroundImage(image.src, viewport.renderAll.bind(viewport));
					}

					if (showModal) {
						ModalService.showModal({
							templateUrl: 'views/templates/modal/yesnomodal.tpl.html',
							controller: 'YesNoModal',
							inputs: {
								title: "WARNING",
								text: "This action will delete all current annotations. Only proceed if you're absolutely sure what you're doing.",
								yesText: "Go ahead.",
								noText: "Go back.",
								yesButtonClass: "btn-danger",
								noButtonClass: "btn-primary"
							}
						}).then(function (modal) {
							modal.element.modal();
							modal.close.then(function (result) {
								if (result) {
									clearFunc();
								}
							})
						});
					} else {
						clearFunc();
					}
				}

				scope.downloadImage = function () {
					let oldTransform = viewport.viewportTransform;

					viewport.viewportTransform = [1, 0, 0, 1, 0, 0];
					window.open((viewport.toDataURL('png')));

					viewport.viewportTransform = oldTransform;
				};

				/**
				 * TOOL DEFINITIONS
				 */
				scope.selectTool = {
					handleUp: function () {
					},
					handleDown: function () {
						scope.selectedObject = viewport.getActiveObject();
					},
					handleMove: function () {
					},
					handleSelection: function () {
						_lockCanvas(false);
					},
					reset: function () {
						scope.selectedObject = null;
					},
					unlock: function () {
					}
				};

				scope.circleTool = {
					active: false,
					tempCircle: null,
					startingPoint: null,
					endPoint: null,

					handleUp: function (event) {
						if (this.active) {
							this.active = false;
							if (this.tempCircle !== null) {
								viewport.remove(this.tempCircle);
							}

							let pointer = viewport.getPointer(event.e);

							// Set endPoint of the rectangle
							this.endPoint = {
								x: Math.round(pointer.x),
								y: Math.round(pointer.y)
							};

							// Get options
							let properties = this._calculateProperties(this);

							// Draw actual rectangle
							let circle = new fabric.Circle({
								fill: 'rgba(255,0,0,0.5)',
								left: properties.left,
								top: properties.top,
								radius: properties.radius,
								selectable: false
								//	originX: 'center', originY: 'center'       #draw from center
							});

							viewport.add(circle);
							viewport.renderAll();
						}
					},

					handleDown: function (event) {
						if (!viewport.getActiveObject()) {
							this.active = true;
							let pointer = viewport.getPointer(event.e);
							this.startingPoint = { x: Math.round(pointer.x), y: Math.round(pointer.y) };
						} else {
							this.active = false;
						}
					},

					handleMove: function (event) {
						if (this.active) {
							if (this.tempCircle) {
								viewport.remove(this.tempCircle);
							}
							let pointer = viewport.getPointer(event.e);

							// Set endPoint of the rectangle
							this.endPoint = { x: pointer.x, y: pointer.y };

							// Get options
							let properties = this._calculateProperties(this);

							// Draw actual rectangle
							this.tempCircle = new fabric.Circle({
								fill: 'rgba(0,0,255,0.5)',
								left: properties.left,
								top: properties.top,
								radius: properties.radius,
								//	originX: 'center', originY: 'center'    #draw from center
							});

							viewport.add(this.tempCircle);
							viewport.renderAll();
						}
					},
					handleSelection: function () {
						_lockCanvas(true);
					},
					reset: function () {
					},

					unlock: function () {
					},

					_calculateProperties: function (self) {
						let left;
						let top;
						let right;
						let bottom;

						// Calculation for x-coordinates
						if (self.endPoint.x >= self.startingPoint.x) {
							left = self.startingPoint.x;
							right = self.endPoint.x;
						} else {
							left = self.endPoint.x;
							right = self.startingPoint.x;
						}

						// Calculation for y-coordinates
						if (self.endPoint.y >= self.startingPoint.y) {
							top = self.startingPoint.y;
							bottom = self.endPoint.y;
						} else {
							top = self.endPoint.y;
							bottom = self.endPoint.y;
						}

						// Calculation for radius
						let distX = right - left;
						let distY = top - bottom;
						let radius = Math.sqrt(Math.pow(distX, 2) + Math.pow(distY, 2)) / 2;

						return { left: left, top: top, radius: radius };
					}
				};

				scope.polygonTool = {
					active: false,
					enableEdgeMove: true,
					tempCircles: [],
					tempLines: [],
					tempLine: null,
					previewCircleSize: polygonCircleSize,
					previewLineWidth: polygonLineWidth,

					handleUp: function (event) {
					},

					handleDown: function (event) {
						// Handle click on first point (if present).
						if (this.tempCircles.length > 0 && viewport.getActiveObject() === this.tempCircles[0]) {
							viewport.remove(this.tempLine);
							this._makePolygon();

							// Delete temps
							this.tempLine = null;
							this.tempCircles = [];
							this.tempLines = [];
							this.active = false;

							// Finally, redraw the canvas.
							viewport.renderAll();
							return;
						}

						// Remember polygon outline.
						if (this.tempLine) {
							this.tempLines.push(this.tempLine);
							viewport.add(this.tempLine);
						}

						// If no object selected, create a new selection point.
						if (!viewport.getActiveObject()) {
							this.active = true;
							let pointer = viewport.getPointer(event.e);

							let circleColor = (this.tempCircles.length <= 0) ? 'rgba(0,255,0,0.5)' : 'rgba(255,0,0,0.5)';
							let circleRadius = (this.tempCircles.length <= 0) ? this.previewCircleSize * 4 : this.previewCircleSize;

							// Draw selection circle
							let newPoint = new fabric.Circle({
								top: Math.round(pointer.y - circleRadius),
								left: Math.round(pointer.x - circleRadius),
								radius: circleRadius,
								fill: circleColor,
								perPixelTargetFind: true
							});

							// Disable zooming/rotation controls on the point
							newPoint.hasControls = false;

							// Update temp canvas view
							viewport.bringToFront(this.tempCircles[0]);
							this.tempCircles.push(newPoint);
							viewport.add(newPoint);
							viewport.renderAll();
						}
					},
					handleMove: function (event) {
						if (this.active) {
							if (this.tempLine) {
								viewport.remove(this.tempLine);
							}
							let pointer = viewport.getPointer(event.e);

							let lastPoint = this.tempCircles[this.tempCircles.length - 1];

							// Calculate coordinates
							let lastPointX = Math.round(lastPoint.left + lastPoint.radius);
							let lastPointY = Math.round(lastPoint.top + lastPoint.radius);
							let pointerX = Math.round(pointer.x);
							let pointerY = Math.round(pointer.y);

							this.tempLine = new fabric.Line([lastPointX, lastPointY, pointerX, pointerY], {
								stroke: 'red',
								selectable: false,
								perPixelTargetFind: true,
								strokeWidth: this.previewLineWidth
							});

							// Move lines behind circles (this makes user interaction easier)
							viewport.add(this.tempLine);
							viewport.bringToFront(this.tempCircles[0]);

							// Finally, redraw the canvas
							viewport.renderAll();
						}
					},
					handleSelection: function () {
						_lockCanvas(true);
						this._unlock();
					},
					reset: function () {
						this.active = false;

						this.tempCircles.forEach(function (tempCircle) {
							viewport.remove(tempCircle);
						});

						this.tempLines.forEach(function (tempLine) {
							viewport.remove(tempLine);
						});

						viewport.remove(this.tempLine);

						this.tempCircles = [];
						this.tempLine = [];
						this.tempLine = null;
					},

					_unlock: function () {
						this.tempCircles.forEach(function (tempCircle) {
							tempCircle.selectable = true;
						});
					},

					_makePolygon: function () {
						let offset = this.previewCircleSize;
						let canvas = viewport;
						let points = [];

						// Remove preview lines from view
						this.tempLines.forEach(function (e) {
							canvas.remove(e);
						});

						// Remove preview points from view but remember their coordinates
						this.tempCircles.forEach(function (e) {
							canvas.remove(e);
							points.push({ x: e.left + offset, y: e.top + offset });
						});

						let polygon = new fabric.Polygon(points, {
							fill: 'rgba(255,0,0,0.5)',
							perPixelTargetFind: true,
							selectable: false
						});

						// Add the polygon to the canvas
						canvas.add(polygon);
					}
				};

				scope.rectangleTool = {
					active: false,
					tempRect: null,
					startingPoint: null,
					endPoint: null,

					handleUp: function (event) {
						if (this.active) {
							if (this.tempRect !== null) {
								viewport.remove(this.tempRect);
							}

							let pointer = viewport.getPointer(event.e);

							// Set endPoint of the rectangle
							this.endPoint = { x: pointer.x, y: pointer.y };

							// Get options
							let properties = this._calculateProperties();

							// Draw actual rectangle
							let rectangle = new fabric.Rect({
								fill: 'rgba(255,0,0,0.5)',
								left: properties.left,
								top: properties.top,
								width: properties.width,
								height: properties.height,
								selectable: false
							});

							this.active = false;

							viewport.add(rectangle);
							viewport.renderAll();
						}
					},

					handleDown: function (event) {
						if (!viewport.getActiveObject()) {
							this.active = true;
							let pointer = viewport.getPointer(event.e);
							this.startingPoint = { x: Math.round(pointer.x), y: Math.round(pointer.y) };
						} else {
							this.active = false;
						}
					},

					handleMove: function (event) {
						if (this.active) {
							if (this.tempRect !== null) {
								viewport.remove(this.tempRect);
							}
							let pointer = viewport.getPointer(event.e);

							// Set endPoint of the rectangle
							this.endPoint = { x: Math.round(pointer.x), y: Math.round(pointer.y) };

							// Get options
							let properties = this._calculateProperties();

							// Draw actual rectangle
							this.tempRect = new fabric.Rect({
								fill: 'rgba(0,0,255,0.5)',
								left: properties.left,
								top: properties.top,
								width: properties.width,
								height: properties.height
							});

							viewport.add(this.tempRect);
							viewport.renderAll();
						}
					},
					handleSelection: function () {
						_lockCanvas(true);
					},
					reset: function () {
					},

					unlock: function () {
					},

					_calculateProperties: function () {
						let left;
						let width;
						let top;
						let height;

						if (this.endPoint.x >= this.startingPoint.x) {
							left = this.startingPoint.x;
							width = this.endPoint.x - this.startingPoint.x;
						} else {
							left = this.endPoint.x;
							width = this.startingPoint.x - this.endPoint.x;
						}

						// Calculation for y-coordinates
						if (this.endPoint.y >= this.startingPoint.y) {
							top = this.startingPoint.y;
							height = this.endPoint.y - this.startingPoint.y;
						} else {
							top = this.endPoint.y;
							height = this.startingPoint.y - this.endPoint.y;
						}

						return { left: left, width: width, top: top, height: height };
					}
				};

				scope.eraserTool = {
					handleUp: function (event) {
					},

					handleDown: function (event) {
						let selected = viewport.getActiveObject();

						if (selected !== null) {
							viewport.remove(viewport.getActiveObject());
						}
					},

					handleMove: function (event) {
					},
					handleSelection: function () {
						_lockCanvas(false);
					},
					reset: function () {
					},

					unlock: function () {
					}
				};

				scope.panTool = {
					active: false,
					lastX: null,
					lastY: null,

					handleUp: function (event) {
						this.active = false;
					},

					handleDown: function (event) {
						if (viewport.getActiveObject() === undefined || viewport.getActiveObject() === null) {
							this.active = true;
						}

						this.lastX = event.e.screenX;
						this.lastY = event.e.screenY;
					},

					handleMove: function (event) {
						if (this.active) {
							let pointer = viewport.getPointer(event.e);

							let curPosX = event.e.screenX;
							let curPosY = event.e.screenY;

							let deltaX = curPosX - this.lastX;
							let deltaY = curPosY - this.lastY;

							this.lastX = curPosX;
							this.lastY = curPosY;

							viewport.relativePan({ x: deltaX, y: deltaY });
							_checkOutOfBounds();

							viewport.renderAll();
						}
					},
					handleSelection: function () {
						_lockCanvas(true);
					},
					reset: function () {
					},

					unlock: function () {
					}
				};

				scope.cloneTool = {
					handleUp: function () {
					},
					handleDown: function () {
						let clone = viewport.getActiveObject().clone((clone) => {
							clone.set('top', clone.top + 5);
							clone.set('left', clone.left + 5);
							viewport.add(clone);
						})
					},
					handleMove: function () {
					},
					handleSelection: function () {
						_lockCanvas(false);
					},
					reset: function () {
						scope.selectedObject = null;
					},
					unlock: function () {
					}
				};

				/**
				 * FEATURE (UNDO / REDO FUNCTIONALITY)
				 * 
				 * We have 3 types of changes in total: add, remove, modify.
				 * Track is kept of all these changes in the undo/redo stacks.
				 */
				scope.undo = function () {
					let undoEvent = undoStack[0];

					if (undoEvent) {
						undoStack.pop(undoEvent);

						switch (undoEvent.action) {
							case "added":
								viewport.remove(undoEvent.obj);
								break;
							case "remvoed":
								viewport.add(undoEvent.obj);
								break;
							case "modified":
								break;
							default:
								console.error("[UNDO] Could not recognize undo action.");
						}
					}
				};

				scope.redo = function () {
					// todo
				};

				viewport.on('object:added', function (event) {
					if (scope.currentTool && !scope.currentTool.active
						&& event && event.target) {
						undoStack.push({
							action: "added",
							obj: event.target
						});
					}
				});

				viewport.on('object:modified', function (event) {
					if (scope.currentTool && !scope.currentTool.active
						&& event && event.target && event.transform) {
						undoStack.push({
							action: "modified",
							obj: event.target,
							transform: event.transform
						});
					}
				});

				viewport.on('object:removed', function (event) {
					if (scope.currentTool && !scope.currentTool.active) {
						undoStack.push({
							action: "removed",
							obj: event.target
						});
					}
				});

				/**
				 * FEATURE (HOTKEYS) FOR TOOL SELECTION)
				 */
				scope.hotkey = function (event) {
					if (event && event.key) {
						switch (event.key) {
							case "1":
								scope.setTool(scope.selectTool);
								break;
							case "2":
								scope.setTool(scope.panTool);
								break;
							case "3":
								scope.setTool(scope.rectangleTool);
								break;
							case "4":
								scope.setTool(scope.polygonTool);
								break;
							case "5":
								//scope.setTool(null);
								break;
							case "6":
								scope.setTool(scope.eraserTool);
								break;
							case "7":
								scope.setTool(scope.cloneTool);
								break;
							case "Delete":
								if (scope.selectedTool && scope.selectedTool.active
									&& scope.selectedTool.handleDelete) {
									scope.selectedTool.handleDelete();
								} else {
									viewport.remove(viewport.getActiveObject());
								}
							default:
						}
					}
				}

				/**
				 * EVENT BINDINGS (CANVAS ACTIONS)
				 */
				viewport.on('mouse:down', function (event) {
					if (scope.currentTool && scope.currentTool.handleDown) {
						scope.currentTool.handleDown(event);
					}

					_pullFabricData();
					scope.$apply();
				});

				viewport.on('mouse:up', function (event) {
					if (scope.currentTool && scope.currentTool.handleUp) {
						scope.currentTool.handleUp(event);
					}
					_pullFabricData();
					scope.$apply();
				});

				viewport.on('mouse:move', function (event) {
					if (scope.currentTool && scope.currentTool.handleMove) {
						scope.currentTool.handleMove(event);
					}

					// Check position of pointer and enable autoscrolling, if requested.
					if (scope.currentTool.enableEdgeMove) {
						let width = viewport.width;
						let height = viewport.height;

						let pointerX = event.pointer.x;
						let pointerY = event.pointer.y;

						if (pointerX < edgeThresholdPixels || pointerY < edgeThresholdPixels
							|| pointerX > width - edgeThresholdPixels
							|| pointerY > height - edgeThresholdPixels) {
							viewport.fire('neoclassica:onEdge', event);
						} else if (viewport.currentlyAutoScrolling) {
							viewport.fire('neoclassica:onEdgeLeave', event);
						}
					}
				});

				viewport.on('mouse:out', function (event) {
					viewport.fire('neoclassica:onEdgeLeave');
				})

				/**
				 * FEATURE (AUTOMATIC SCROLLING)
				 */

				let automaticScrollingInterval = null;

				let automaticScrolling = function(deltaX, deltaY) {
					viewport.relativePan({ x: deltaX, y: deltaY });
					_checkOutOfBounds();
				}

				viewport.on('neoclassica:onEdge', function (mouseEvent) {
					viewport.currentlyAutoScrolling = true;

					// Work out what edge we're in.
					let top, right, bottom, left;
					let topDist = Infinity, rightDist = Infinity, bottomDist = Infinity, leftDist = Infinity;

					if (mouseEvent.pointer.x < edgeThresholdPixels) {
						left = true;
						leftDist = mouseEvent.pointer.x;
					}

					if (mouseEvent.pointer.y < edgeThresholdPixels) {
						top = true;
						topDist = mouseEvent.pointer.y;
					}

					if (mouseEvent.pointer.x > viewport.width - edgeThresholdPixels) {
						right = true;
						rightDist = viewport.width - mouseEvent.pointer.x;
					}

					if (mouseEvent.pointer.y > viewport.height - edgeThresholdPixels) {
						bottom = true;
						bottomDist = viewport.height - mouseEvent.pointer.y;
					}

					// Calculate scroll factor based on distance from the nearest edge.
					let minDist = Math.min(topDist, rightDist, leftDist, bottomDist);
					let minDistPercent = 1 - minDist / edgeThresholdPixels;

					let scrollFactor = Math.min(autoScrollSpeedCapPixels, Math.exp(minDistPercent * 3.5));

					// Determine scroll direction.
					let scrollDirection = {x: 0, y: 0};

					if (top) {
						scrollDirection.y = 1 * scrollFactor;
					}

					if (bottom) {
						scrollDirection.y = -1 * scrollFactor;
					}

					if (left) {
						scrollDirection.x = 1 * scrollFactor;
					}

					if (right) {
						scrollDirection.x = -1 * scrollFactor;
					}

					// Scroll repeatedly.
					clearInterval(automaticScrollingInterval);

					automaticScrollingInterval = setInterval(function() {
						automaticScrolling(scrollDirection.x, scrollDirection.y);
					}, 33);
				});

				viewport.on('neoclassica:onEdgeLeave', function (mouseEvent) {
					clearInterval(automaticScrollingInterval);
					viewport.currentlyAutoScrolling = false;
				});

				/**
				 * EVENT BINDINGS (ZOOMING)
				 */
				viewport.on('mouse:wheel', function (event) {
					let delta = event.e.deltaY * -1;
					let x = event.e.clientX;
					let y = event.e.clientY;

					let currentZoom = viewport.getZoom();
					let newZoomLevelCandidate = currentZoom + delta / 500; // TODO: Factor in image size here!
					let newZoomLevel = (newZoomLevelCandidate <= minScaleFactor) ? minScaleFactor : newZoomLevelCandidate;

					viewport.zoomToPoint({ x: x, y: y }, newZoomLevel);
					_checkOutOfBounds();
					return true;
				});

				document.getElementById('annotation-container').onwheel = function (event) {
					event.preventDefault();
				};

				document.getElementById('annotation-container').onmousewheel = function (event) {
					event.preventDefault();
				};

				document.getElementById('annotation-container').mousewheel = function (event) {
					event.preventDefault();
				};

				/**
				 * EVENT BINDINGS (WINDOW RESIZE)
				 */
				$(window).resize(function () {
					this.setTimeout(_computeCanvasSize, 300);
				})

				/**
				 * EVENT BINDINGS (REMOTE DATA HANDLING)
				 */
				scope.$watchCollection('annotations', function (newAnnotations) {
					if (!initialDataLoaded && newAnnotations) {
						initialDataLoaded = true;
						scope.clear(false);
						_restoreAnnotations(newAnnotations);

						// Mark annotations as imported.
						scope.importResult = {success: true};
					}
				});

				scope.$watch('image', function (newImage) {
					if (newImage) {
						image = new Image();
						image.src = newImage;
						image.crossOrigin = 'Anonymous';

						image.onload = function () {
							viewport.setZoom(1);
							viewport.setBackgroundImage(image.src, viewport.renderAll.bind(viewport));
							_computeCanvasSize();
						};
					}
				});

				/**
				 * HELPER FUNCTIONS
				 */
				const _pullFabricData = function () {
					scope.annotations = viewport.getObjects();
				};

				const _restoreAnnotations = function (newAnnotations) {
					// Convert annotation data to fabric
					newAnnotations.forEach(function (obj) {
						let objAdd = null;
						if (obj.type === 'rect') {
							objAdd = new fabric.Rect(obj);
						} else if (obj.type === 'circle') {
							objAdd = new fabric.Circle(obj);
						} else if (obj.type === 'polygon') {
							objAdd = new fabric.Polygon(obj.points, obj);
							objAdd.perPixelTargetFind = true;
						} else {
							console.error('Could not identify object.');
						}

						objAdd.id = obj.id;
						objAdd.className = obj.className;

						viewport.add(objAdd);
					});
					viewport.renderAll();
				};

				const _getCanvasContainerDimensions = function () {
					let container = document.getElementById('annotation-container');
					return {
						width: container.clientWidth,
						height: container.clientHeight
					}
				}

				const _computeCanvasSize = function () {
					let containerWidth = _getCanvasContainerDimensions().width;
					let containerHeight = window.innerHeight - 175;

					// If image width is wider than container, calculate zoom factor
					let zoomFactor = 1;

					if (image.width > containerWidth) {
						zoomFactor = containerWidth / image.width;
					} else if (image.height > containerHeight) {
						zoomFactor = containerHeight / image.height;
					}

					viewport.setWidth(Math.floor(image.width * zoomFactor));
					viewport.setHeight(Math.floor(image.height * zoomFactor));
					viewport.setZoom(zoomFactor);

					minScaleFactor = zoomFactor;
				};

				const _lockCanvas = function (flag) {
					viewport.forEachObject(function (object) {
						object.selectable = !flag;
					});
				};

				const _checkOutOfBounds = function () {
					// Get transform matrix of viewport.
					let vpTransform = viewport.viewportTransform;

					let imgHeightScaled = viewport.height / vpTransform[0];
					let imgWidthScaled = viewport.width / vpTransform[0];

					let imgWidth = image.width;
					let imgHeight = image.height;

					// Reset pan, if necessary.
					if (vpTransform[4] > 0) {
						vpTransform[4] = 0;
					}

					if (vpTransform[5] > 0) {
						vpTransform[5] = 0;
					}

					// Boundaries on the right.
					if (imgWidthScaled + -1 * vpTransform[4] / vpTransform[0] > imgWidth) {
						vpTransform[4] = -1 * (imgWidth - imgWidthScaled) * vpTransform[0];
					}

					// Boundaries on the bottom.
					if (imgHeightScaled + -1 * vpTransform[5] / vpTransform[0] > imgHeight) {
						vpTransform[5] = -1 * (imgHeight - imgHeightScaled) * vpTransform[0];
					}

					viewport.viewportTransform = vpTransform;
				}

				// Default to select tool.
				scope.setTool(scope.selectTool);
			}
		};
	}]);
