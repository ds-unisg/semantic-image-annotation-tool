'use strict';

/**
 * @ngdoc directive
 * @name AnnotationToolApp.directive:annotationsViewer
 * @description
 * # annotationsViewer
 */
angular.module('AnnotationToolApp')
	.directive('annotationsViewer', [function () {
		return {
			templateUrl: 'views/templates/annotations_viewer_tpl.html',
			restrict: 'E',
			scope: {
				ontologyClass: '='
			},
			link: function postLink(scope, element, attrs) {
			}
		};
	}]);
