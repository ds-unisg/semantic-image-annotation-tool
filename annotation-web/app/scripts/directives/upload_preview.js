'use strict';

/**
 * @ngdoc directive
 * @name AnnotationToolApp.directive:uploadPreview
 * @description
 * # uploadPreview
 */
angular.module('AnnotationToolApp')
	.directive('uploadPreview', function () {
		return {
			template: '<img style="max-height: 100px;"></img>',
			restrict: 'A',
			link: function postLink(scope, element, attrs) {
				let params = scope.$eval(attrs.uploadPreview);

				let image = element.find("img")[0];

				let fr = new FileReader();

				fr.onload = function (event) {
					image.src = event.target.result;
				};
				console.log(params);
				fr.readAsDataURL(params.file);
			}
		};
	});
