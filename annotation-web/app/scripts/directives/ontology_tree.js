'use strict';

/**
 * @ngdoc directive
 * @name AnnotationToolApp.directive:ontologyTree
 * @description
 * # ontologyTree
 */
angular.module('AnnotationToolApp')
	.directive('ontologyTree', function () {
		return {
			template:
			'<div class="scrollwrapper">' +
			'<ul>' +
			'<li ng-repeat="class in ontology.ontologyChildren" style="padding-left: 2px">' +
			'<ontology-element ontology-class="class" click-handler="selected" selected-language="selectedLanguage"></ontology-element>' +
			'</li>' +
			'</ul>' +
			'</div>',
			restrict: 'E',
			scope: {
				ontology: '=',
				selected: '=',
				selectedLanguage: '='
			},
			link: function postLink(scope) {
			}
		};
	});
